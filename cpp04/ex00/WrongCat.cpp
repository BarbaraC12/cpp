#include "WrongCat.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

WrongCat::WrongCat() {

	this->_type = "WrongCat";
	std::cout << "\033[0;32m>> \033[0mType " << this->_type << " has been assigned." << std::endl;
}

WrongCat::WrongCat( const WrongCat & src ) {

	*this = src;
	std::cout << "New WrongCat by copy is " << this->_type << std::endl;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

WrongCat::~WrongCat() {
	
	std::cout << "\033[0;31m>> \033[0mWrong Cat left without make grabage" << std::endl;
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

WrongCat &				WrongCat::operator=( WrongCat const & rhs ) {

	if ( this != &rhs )
	{
		this->_type = rhs.getType();
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, WrongCat const & i ) {

	o << "Type = " << i.getType();
	return o;
}


/*
** --------------------------------- METHODS ----------------------------------
*/

void					WrongCat::makeSound( void ) const {

	std::cout << "Wrong Cat say: MEOOOOOWWW! I'm a fake cat." << std::endl;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

std::string				WrongCat::getType( void ) const {

	return this->_type;
}

/* ************************************************************************** */