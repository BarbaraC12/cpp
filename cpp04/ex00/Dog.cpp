#include "Dog.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Dog::Dog() :Animal("Dog") {

	// this->_type = "Dog";
	std::cout << "\033[0;32m>> \033[0mType " << this->_type << " has been assigned." << std::endl;
}

Dog::Dog( const Dog & src ) {

	*this = src;
	std::cout << "New Dog by copy is " << this->_type << std::endl;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Dog::~Dog() {

	std::cout << "\033[0;31m>> \033[0mDog is going to search a ball" << std::endl;
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Dog &				Dog::operator=( Dog const & rhs )
{
	if ( this != &rhs )
	{
		this->_type = rhs.getType();
	}
	return *this;
}

std::ostream &		operator<<( std::ostream & o, Dog const & i )
{
	o << "Type = " << i.getType();
	return o;
}


/*
** --------------------------------- METHODS ----------------------------------
*/

void				Dog::makeSound( void ) const {

	std::cout << "Animal " << this->_type << " say: WOOF! I'm a good boy." << std::endl;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

std::string				Dog::getType( void ) const {

	return this->_type;
}

/* ************************************************************************** */