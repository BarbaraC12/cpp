#ifndef DOG_HPP
# define DOG_HPP

# include "Animal.hpp"
# include <iostream>
# include <string>

class Dog :virtual public Animal {

	public:

		Dog();
		Dog( Dog const & src );
		~Dog();

		virtual void	makeSound( void ) const;
		std::string		getType( void ) const;
		Dog &			operator=( Dog const & rhs );

	private:

};

std::ostream &			operator<<( std::ostream & o, Dog const & i );

#endif /* ************************************************************* DOG_H */