#ifndef ANIMAL_HPP
# define ANIMAL_HPP

# include <iostream>
# include <string>

class Animal
{

	public:

		Animal( void );
		Animal( std::string type );
		Animal( Animal const & src );
		virtual ~Animal( void );

		virtual void	makeSound( void ) const;
		std::string		getType( void ) const;
		Animal &		operator=( Animal const & rhs );

	protected:
		std::string		_type;

};

std::ostream &			operator<<( std::ostream & o, Animal const & i );

#endif /* ********************************************************** ANIMAL_H */