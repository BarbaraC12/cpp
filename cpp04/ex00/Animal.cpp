#include "Animal.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Animal::Animal( void ) :_type("Unknow") {
	std::cout << "\033[0;32m> \033[0mAnimal type " << this->_type << " has been created." << std::endl;
}

Animal::Animal( std::string type ) :_type(type) {
	std::cout << "\033[0;32m> \033[0mAnimal type " << this->_type << " has been created." << std::endl;
}

Animal::Animal( const Animal & src ) {

	*this = src;
	std::cout << "New Animal by copy is " << this->_type << std::endl;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Animal::~Animal( void ) {

	std::cout << "\033[0;31m> \033[0mAnimal left without make grabage" << std::endl;
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Animal &				Animal::operator=( Animal const & rhs )
{
	if ( this != &rhs )
	{
		this->_type = rhs.getType();
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, Animal const & i )
{
	o << "Type = " << i.getType();
	return o;
}

/*
** --------------------------------- METHODS ----------------------------------
*/

void					Animal::makeSound( void ) const {

	std::cout << "Animal " << this->_type << " say: Ggggrrraouuuu!" << std::endl;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

std::string				Animal::getType( void ) const {

	return this->_type;
}

/* ************************************************************************** */