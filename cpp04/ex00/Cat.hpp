#ifndef CAT_HPP
# define CAT_HPP

# include "Animal.hpp"
# include <iostream>
# include <string>

class Cat :virtual public Animal {

	public:

		Cat();
		Cat( Cat const & src );
		~Cat();

		virtual void	makeSound( void ) const;
		std::string		getType( void ) const;
		Cat &			operator=( Cat const & rhs );

	private:

};

std::ostream &			operator<<( std::ostream & o, Cat const & i );

#endif /* ************************************************************* CAT_H */