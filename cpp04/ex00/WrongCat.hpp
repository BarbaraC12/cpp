#ifndef WRONGCAT_HPP
# define WRONGCAT_HPP

# include <iostream>
# include <string>
# include "WrongAnimal.hpp"

class WrongCat :virtual public WrongAnimal {

	public:

		WrongCat();
		WrongCat( WrongCat const & src );
		~WrongCat();

		void			makeSound( void ) const;
		std::string		getType( void ) const;
		WrongCat &		operator=( WrongCat const & rhs );

	protected:
		std::string		_type;

};

std::ostream &			operator<<( std::ostream & o, WrongCat const & i );

#endif /* ******************************************************** WRONGCAT_H */