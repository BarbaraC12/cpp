#include "Cat.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Cat::Cat() :Animal("Cat") {

	// this->_type = "Cat";
	std::cout << "\033[0;32m>> \033[0mType " << this->_type << " has been assigned." << std::endl;
}

Cat::Cat( const Cat & src ) {

	*this = src;
	std::cout << "New Cat by copy is " << this->_type << std::endl;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Cat::~Cat() {

	std::cout << "\033[0;31m>> \033[0mCat is totally ignoring you and gone" << std::endl;
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Cat &				Cat::operator=( Cat const & rhs )
{
	if ( this != &rhs )
	{
		this->_type = rhs.getType();
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, Cat const & i )
{
	o << "Type = " << i.getType();
	return o;
}

/*
** --------------------------------- METHODS ----------------------------------
*/

void					Cat::makeSound( void ) const {

	std::cout << "Animal " << this->_type << " say: MEOOOOOWWW! Slave feed me!" << std::endl;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

std::string				Cat::getType( void ) const {

	return this->_type;
}

/* ************************************************************************** */