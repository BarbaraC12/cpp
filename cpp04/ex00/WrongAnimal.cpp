#include "WrongAnimal.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

WrongAnimal::WrongAnimal() :_type("Unknow") {
	std::cout << "\033[0;32m> \033[0mAnimal type " << this->_type << " has been created." << std::endl;
}

WrongAnimal::WrongAnimal( const WrongAnimal & src ) {

	*this = src;
	std::cout << "New Animal by copy is " << this->_type << std::endl;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

WrongAnimal::~WrongAnimal() {

	std::cout << "\033[0;31m> \033[0mWrong Animal left without make grabage" << std::endl;
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

WrongAnimal &				WrongAnimal::operator=( WrongAnimal const & rhs ) {

	if ( this != &rhs )
	{
		this->_type = rhs.getType();
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, WrongAnimal const & i ) {

	o << "Type = " << i.getType();
	return o;
}


/*
** --------------------------------- METHODS ----------------------------------
*/

void					WrongAnimal::makeSound( void ) const {

	std::cout << "WrongAnimal " << this->_type << " say: Hey! I'm a wrong sound." << std::endl;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

std::string				WrongAnimal::getType( void ) const {

	return this->_type;
}

/* ************************************************************************** */