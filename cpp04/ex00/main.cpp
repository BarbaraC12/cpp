#include <iostream>
#include "Dog.hpp"
#include "Cat.hpp"
#include "Animal.hpp"
#include "WrongCat.hpp"
#include "WrongAnimal.hpp"


int main()
{
    std::cout << "\033[1;33m\t##########  Polymorphisme  ########## \033[0m" << std::endl;
	{
		std::cout << std::endl;
    	std::cout << "\033[0;33m######  Animal  ###### \033[0m" << std::endl;
		const Animal* meta = new Animal();
		const Animal* j = new Dog();
		const Animal* i = new Cat();
		std::cout << j->getType() << " " << std::endl;
		std::cout << i->getType() << " " << std::endl;
		i->makeSound(); //will output the cat sound!
		j->makeSound();
		meta->makeSound();

		delete j;
		delete i;
		delete meta;
	}
	{
		std::cout << std::endl;
		std::cout << "\033[0;33m######  Wrong Animal  ###### \033[0m" << std::endl;
		const WrongAnimal* meta = new WrongAnimal();
		const WrongAnimal* i = new WrongCat();
		const WrongCat* j = new WrongCat();
		
		i->makeSound(); //will not output the cat sound!
		j->makeSound(); //will output the cat sound!
		meta->makeSound();

		delete i;
		delete j;
		delete meta;
	}
	
	return 0;
}