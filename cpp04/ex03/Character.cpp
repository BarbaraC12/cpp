#include "Character.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Character::Character( void ) :_name("Pouet"), _nbMateria(0), _nbDelMateria(0) {

}

Character::Character( std::string name ) :_name(name), _nbMateria(0), _nbDelMateria(0) {

}

Character::Character( const Character & src ) {

	if (this != &src)
	{
		this->_name = src._name;
		this->_nbDelMateria = src._nbDelMateria;
		this->_nbMateria = src._nbMateria;
		for (int i = 0; i < this->_nbMateria; i++) {

			this->_inventory[i] = src._inventory[i]->clone();
		}
		for (int i = 0; i < this->_nbDelMateria; i++) {

			this->_delMateria[i] = src._delMateria[i]->clone();
		}
	}
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Character::~Character() {

	while (this->_nbMateria > 0) {
		delete this->_inventory[this->_nbMateria - 1];
		this->_nbMateria--;
	}
	for (int i = 0; i < _nbDelMateria; i++) {
		delete this->_delMateria[i];
		this->_delMateria[i] = NULL;
	}
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Character &				Character::operator=( Character const & rhs )
{
	if (this != &rhs)
	{
		int		i;
		i = 0;
		while (i < this->_nbMateria)
		{
			delete this->_inventory[i];
			i++;
		}
		this->_name = rhs._name;
		this->_nbMateria = rhs._nbMateria;
		this->_nbDelMateria = rhs._nbDelMateria;
		i = 0;
		while (i < this->_nbMateria)
		{
			this->_inventory[i] = rhs._inventory[i]->clone();
			i++;
		}
		i = 0;
		while (i < this->_nbDelMateria)
		{
			this->_delMateria[i] = rhs._delMateria[i]->clone();
			i++;
		}
	}
	return *this;;
}

/*
** --------------------------------- METHODS ----------------------------------
*/

void					Character::equip( AMateria * m )
{
	if (this->_nbMateria < NB_MATERIA && m != NULL)
	{
		this->_inventory[this->_nbMateria] = m->clone();
		this->_nbMateria++;
		if (this->_nbDelMateria == NB_MATERIA)
		{
			for (int i = 0; i < _nbDelMateria; i++)
			{
				delete this->_delMateria[i];
				this->_delMateria[i] = NULL;
			}
			this->_nbDelMateria = 0;
		}
	}
	else
		std::cout << "Inventory full, cannot equip a new Materia" << std::endl;
}

void					Character::unequip( int idx )
{
	if (idx >= 0 && idx < this->_nbMateria)
	{
		int		i;
		i = idx;
		this->_delMateria[this->_nbDelMateria] = this->_inventory[idx];
		this->_nbDelMateria++;
		while (i < this->_nbMateria - 1)
		{
			this->_inventory[i] = this->_inventory[i + 1];
			i++;
		}
		this->_inventory[i] = NULL;
		this->_nbMateria--;
	}
	else
		std::cout << "Invalid index given for unequip function" << std::endl;
}

void					Character::use( int idx, ICharacter & target )
{
	if (idx >= 0 && idx < _nbMateria)
	{
		this->_inventory[idx]->use(target);
	}
	else
		std::cout << "Invalid index given for use function on target: " << target.getName() << std::endl;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

const std::string &		Character::getName( void ) const {

	return this->_name;
}


/* ************************************************************************** */