#include "MateriaSource.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

MateriaSource::MateriaSource( void ) :_nbMateria(0) {

}

MateriaSource::MateriaSource( const MateriaSource& src ) {

	if (this != &src) {

		int		i(0);
		this->_nbMateria = src._nbMateria;
		while (i < this->_nbMateria) {
			this->_materiaTab[i] = src._materiaTab[i]->clone();
			i++;
		}
	}
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

MateriaSource::~MateriaSource() {

	while (this->_nbMateria > 0) {

		delete this->_materiaTab[this->_nbMateria - 1];
		this->_nbMateria--;
	}
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

MateriaSource &				MateriaSource::operator=( MateriaSource const & rhs ) {
	
	if (this != &rhs) {
		int		i(0);
		while (i < this->_nbMateria) {
	
			delete _materiaTab[i];
			i++;
		}
		i = 0;
		while (i < rhs._nbMateria) {

			_materiaTab[i] = rhs._materiaTab[i]->clone();
			i++;
		}
	}
	return *this;;
}

/*
** --------------------------------- METHODS ----------------------------------
*/



void				MateriaSource::learnMateria( AMateria * src ) {

	if (this->_nbMateria < 4) {
		this->_materiaTab[this->_nbMateria] = src;
		this->_nbMateria++;
	}
}

AMateria *			MateriaSource::createMateria( const std::string & type ) {

	int		i(0);
	while (i < this->_nbMateria) {
		if (_materiaTab[i]->getType() == type)
			return this->_materiaTab[i];
		i++;
	}
	return 0;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/


/* ************************************************************************** */