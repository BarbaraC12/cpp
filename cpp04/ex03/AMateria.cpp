#include "AMateria.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

AMateria::AMateria( void ) {

}

AMateria::AMateria( const std::string & type ): _type(type) {

}

AMateria::AMateria( const AMateria & src ) :_type(src.getType() ) {
	
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

AMateria::~AMateria() {

}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

AMateria &				AMateria::operator=( const AMateria & rhs )
{
	(void)rhs;
	return *this;
}

/*
** --------------------------------- METHODS ----------------------------------
*/

void					AMateria::use( ICharacter & target )
{
	std::cout << "use called on " << target.getName() << std::endl;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

const std::string &		AMateria::getType( void ) const
{
	return (this->_type);
}

/* ************************************************************************** */