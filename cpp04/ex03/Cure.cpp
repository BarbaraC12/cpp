# include "Cure.hpp"

Cure::Cure( void ): AMateria("Cure") {

}

Cure::Cure( const Cure & src ): AMateria(src) {

}

Cure::~Cure( void ) {
	
}

Cure &			Cure::operator=( const Cure & rhs ){

	(void)rhs;
	return *this;
}

Cure *			Cure::clone( void ) const {

	Cure *	clone = new Cure();
	return clone;
}

void			Cure::use( ICharacter & target ) {

	std::cout << "* heals " << target.getName() << "'s wounds *" << std::endl;
}
