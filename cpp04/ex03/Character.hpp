#ifndef Character_HPP
# define Character_HPP

#include "ICharacter.hpp"
#include <string>
#include <iostream>

#define NB_MATERIA	4

class Character;
class AMateria;

class Character: public ICharacter
{
private:
	std::string		_name;
	AMateria *		_inventory[NB_MATERIA];
	int				_nbMateria;
	AMateria *		_delMateria[NB_MATERIA];
	int				_nbDelMateria;

public:
	Character( void );
	Character( std::string name );
	Character( const Character & src );
	~Character( void );

	Character &		operator=( const Character & rhs );

	const std::string &		getName( void ) const;
	void					equip( AMateria* m );
	void					unequip( int idx );
	void					use( int idx, ICharacter & target );
};

#endif /* ****************************************************** Character_H */