# include "Ice.hpp"

Ice::Ice( void ): AMateria("ice") {
}

Ice::Ice( const Ice & src ): AMateria(src) {

}

Ice::~Ice( void ) {
}

Ice &			Ice::operator=( const Ice & rhs ) {

	(void)rhs;
	return *this;
}

Ice *			Ice::clone( void ) const {

	Ice *	clone = new Ice();
	return clone;
}

void			Ice::use( ICharacter & target ) {

	std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
}
