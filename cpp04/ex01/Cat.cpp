#include "Cat.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Cat::Cat() :Animal() {

	this->_type = "Cat";
	std::cout << "\033[0;32mType " << this->_type << " has been assigned.\033[0m" << std::endl;
	this->_brain = new Brain();
}

Cat::Cat( const Cat & src ) {

	this->_brain = new Brain(*src._brain);
	this->_type = src.getType();
	std::cout << "New Cat by copy, is type are: " << this->_type << std::endl;
}

/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Cat::~Cat() {

	if (this->_brain)
		delete	this->_brain;
	std::cout << "\033[0;31mAnonymised cat\033[0m" << std::endl;
}

/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Cat &				Cat::operator=( Cat const & rhs )
{
	if ( this != &rhs )
	{
		this->_type = rhs.getType();
		if (_brain)
			delete _brain;
		Brain _brain(*rhs._brain);
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, Cat const & i )
{
	o << "Type = " << i.getType();
	o << "Brain = " << i.getBrain();
	return o;
}

/*
** --------------------------------- METHODS ----------------------------------
*/

void					Cat::makeSound( void ) const {

	std::cout << "Animal " << this->_type << " say: HUMMEOOOWNN FEED ME." << std::endl;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

std::string				Cat::getType( void ) const {

	return this->_type;
}

Brain*					Cat::getBrain( void ) const {

	return this->_brain;
}

/* ************************************************************************** */