#include "Brain.hpp"
#include <cstdlib>
#include <ctime>

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Brain::Brain() {

	std::cout << "\033[0;32mNew brain attribut\033[0m" << std::endl;

	for (int i(0); i < 100; i++)
		ideas[i] = "This is an idea";
}

Brain::Brain( const Brain & src ) {

	*this = src;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Brain::~Brain() {

	std::cout << "\033[0;31mBrain destroy\033[0m" << std::endl;
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Brain &				Brain::operator=( Brain const & rhs ) {

	if (this != &rhs)
	{
		for (int i(0); i < 100; i++)
			this->setIdea( i, rhs.getIdea( i ));
	}
	return (*this);
}


/*
** --------------------------------- METHODS ----------------------------------
*/


/*
** --------------------------------- ACCESSOR ---------------------------------
*/

std::string		Brain::getIdea( int i ) const {

	if ( i >= 0 && i < 100)
		return this->ideas[i];
	return "Out of ideas";
}

void			Brain::setIdea( int i, std::string idea ) {

	if ( i >= 0 && i < 100)
		this->ideas[i] = idea;
}

/* ************************************************************************** */