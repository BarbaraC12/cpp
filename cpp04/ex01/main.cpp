#include "Animal.hpp"
#include "Cat.hpp"
#include "Dog.hpp"
#include "Brain.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

#define ARRAY_L	4

// static 
// std::string ideaGenerator( void ) {

// 	srand(time(NULL));
// 	static const std::string idea[22] = {
// 		"Brocolis", "Viande", "Croquette", "Souris", "Chat",
// 		"Copain", "Baballe", "Ecureille", "Oiseau", "Poison",
// 		"Lait", "Paté", "Chien", "Faim", "Ennuie", "Miaou",
// 		"Promenade", "Manger", "Pipi", "Peur", "Fatigue", "Woof"};

// 	return (idea[std::rand() % 22]);
// }

int main() {
	
		std::cout << "\033[1;33m\t##########  Basics ########## \033[0m" << std::endl;
	{
		std::cout << "\033[1;37mCreation of 3 animals ----- \033[0m" << std::endl;

		const Animal*	j = new Dog();
		const Animal*	i = new Cat();

		std::cout << "\033[1;37mAnimals make some sound ----- \033[0m" << std::endl;

		i->makeSound();
		j->makeSound();

		std::cout << "\033[1;37mDeletion of the 3 animals ----- \033[0m" << std::endl;

		delete j;
		delete i;
	}
		std::cout << "\033[1;33m\t########## Copy ########## \033[0m" << std::endl;
	{
		std::cout << "\033[1;37mCreation of a cat and is copy ----- \033[0m" << std::endl;

		const Cat*		babyCat = new Cat();
		const Cat*		copy = new Cat(*babyCat);

		std::cout << "\033[1;37mPrint the adress of their brain ----- \033[0m" << std::endl;

		std::cout << "Adress to babyCat brain = " << &(*(babyCat->getBrain())) << std::endl; 
		std::cout << "Adress to copy babyCat brain = " << &(*(copy->getBrain())) << std::endl; 

		std::cout << "\033[1;37mDeletion of cat and his copy ----- \033[0m" << std::endl;

		delete copy;
		delete babyCat;
	}
		std::cout << std::endl;
	{
		std::cout << "\033[1;37mCreation of a dog and is copy ----- \033[0m" << std::endl;

		Dog		babyDog;
		Dog		copy = babyDog;

		std::cout << "\033[1;37mPrint the adress of their brain ----- \033[0m" << std::endl;

		std::cout << "Adress to babyDog brain = " << babyDog.getBrain() << std::endl; 
		std::cout << "Adress to copy babyDog brain = " << copy.getBrain() << std::endl; 

		std::cout << "\033[1;37mDeletion of babyDog and his copy ----- \033[0m" << std::endl;
	}
		std::cout << "\033[1;33m\t##########  Array of N animals ########## \033[0m" << std::endl;
	{
		std::cout << "\033[1;37mCreation of an array of 4 animals (2 dog/2 cat) ----- \033[0m" << std::endl;

		const Animal*	meute[ARRAY_L];

		for ( int i(0); i < ARRAY_L; i++ ) {
			std::cout << i+1 << ". ";
			if ( i % 2 )
				meute[i] = new Cat;
			else
				meute[i] = new Dog;
		}

		std::cout << "\033[1;37mDeletion of the array ----- \033[0m" << std::endl;

		for ( int i(0); i < ARRAY_L; i++ ) {
			std::cout << i+1 << ". ";
			delete meute[i];
		}
	}
	return 0;
}