#ifndef BRAIN_HPP
# define BRAIN_HPP

# include <iostream>
# include <string>

class Brain
{

	public:

		Brain();
		Brain( Brain const & src );
		virtual ~Brain();

		std::string		getIdea( int i ) const;
		void			setIdea( int i, std::string idea );

		Brain &			operator=( Brain const & rhs );

	protected:	
		std::string		ideas[100];

};

#endif /* *********************************************************** BRAIN_H */