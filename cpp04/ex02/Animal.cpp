#include "Animal.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Animal::Animal( void ) :_type("Unknow") {
	std::cout << "\033[1;32mAAnimal type " << this->_type << " has been created.\033[0m" << std::endl;
}

Animal::Animal( const Animal & src ) {

	*this = src;
	std::cout << "New AAnimal by copy is " << this->_type << std::endl;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Animal::~Animal( void ) {

	std::cout << "\033[1;31mAAnimal die\033[0m" << std::endl;
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Animal &				Animal::operator=( Animal const & rhs )
{
	if ( this != &rhs )
	{
		this->_type = rhs.getType();
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, Animal const & i )
{
	o << "Type = " << i.getType();
	return o;
}

/*
** --------------------------------- METHODS ----------------------------------
*/

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

std::string				Animal::getType( void ) const {

	return this->_type;
}

/* ************************************************************************** */