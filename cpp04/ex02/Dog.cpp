#include "Dog.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Dog::Dog() :Animal() {

	this->_type = "Dog";
	std::cout << "\033[0;32mType " << this->_type << " has been assigned.\033[0m" << std::endl;
	this->_brain = new Brain();
}

Dog::Dog( const Dog & src ) {

	this->_brain = new Brain(*src._brain);
	this->_type = src.getType();
	std::cout << "New Dog by copy, is type are: " << this->_type << std::endl;
}

/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Dog::~Dog() {

	if (this->_brain)
		delete	this->_brain;
	std::cout << "\033[0;31mAnonymised dog\033[0m" << std::endl;
}

/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Dog &				Dog::operator=( Dog const & rhs )
{
	if ( this != &rhs )
	{
		this->_type = rhs.getType();
		if (_brain)
			delete _brain;
		_brain = new Brain(*rhs._brain);
	}
	return *this;
}

std::ostream &		operator<<( std::ostream & o, Dog const & i )
{
	o << "Type = " << i.getType();
	return o;
}


/*
** --------------------------------- METHODS ----------------------------------
*/

void				Dog::makeSound( void ) const {

	std::cout << "Animal " << this->_type << " say: WOOF! I'm a good boy." << std::endl;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

std::string				Dog::getType( void ) const {

	return this->_type;
}

Brain*					Dog::getBrain( void ) const {
	
	return this->_brain;
}

/* ************************************************************************** */