#include "Bureaucrat.hpp"

int		main()
{
	std::cout << "\033[1;33m\t########## Basics ########## \033[0m" << std::endl;
	{
		try	{

			Bureaucrat	Stagiaire;
			std::cout << Stagiaire << std::endl;
			Stagiaire.upGrade();
		}
		catch(std::exception & error) {

			std::cout << error.what() << std::endl;
		}
	}

	std::cout << "\033[1;33m\t########## Test down at lvl 150 ########## \033[0m" << std::endl;
	{
		try	{

			Bureaucrat	Paul("Polo", 150);
			std::cout << Paul << std::endl;
			Paul.downGrade();
			std::cout << Paul << std::endl;
			Paul.upGrade();
		}
		catch(std::exception & error) {

			std::cout << error.what() << std::endl;
		}
	}

	std::cout << "\033[1;33m\t########## Test up at lvl 1 ########## \033[0m" << std::endl;
	{	
		try {

			Bureaucrat	Nicolas("Nicolas", 1);
			std::cout << Nicolas << std::endl;
			Nicolas.upGrade();
			std::cout << Nicolas << std::endl;
			Nicolas.downGrade();
	}
		catch(std::exception & error) {

			std::cout << error.what() << std::endl;
		}
	}

	std::cout << "\033[1;33m\t########## Creation to hight lvl 0 ########## \033[0m" << std::endl;
	{
		try {

			Bureaucrat	Bcano("Barbara", 0);
			throw Bureaucrat::OupsSomethingWrong();
		}
		catch(std::exception & error) {

			std::cout << error.what() << std::endl;
		}
	}
}