#include "PresidentialPardonForm.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

PresidentialPardonForm::PresidentialPardonForm( void ) :Form("PresidentialPardon", 25, 5), _target("undefined") {

}

PresidentialPardonForm::PresidentialPardonForm( std::string target ) :Form("PresidentialPardon", 25, 5), _target(target) {

}

PresidentialPardonForm::PresidentialPardonForm( const PresidentialPardonForm & src ) : Form("PresidentialPardon", 25, 5), _target(src._target) {

}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

PresidentialPardonForm::~PresidentialPardonForm( void ) {

}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

PresidentialPardonForm &			PresidentialPardonForm::operator=( PresidentialPardonForm const & rhs ) {
	if ( this != &rhs ) {

		this->_target = rhs._target;
		setSign(rhs.getIfSigned());
	}
	return *this;
}

/*
** --------------------------------- METHODS ----------------------------------
*/

void								PresidentialPardonForm::executeForm( void ) const {

	std::cout << this->_target << " has been pardon by Zafod Beeblebrox." << std::endl;

}

/* ************************************************************************** */