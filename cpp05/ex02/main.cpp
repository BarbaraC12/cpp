#include "Bureaucrat.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"


int			main( void )
{
	std::cout << "\033[1;37mBureaucrat creation\033[0m" << std::endl;
	Bureaucrat		indic("Meyer", 120);
	Bureaucrat		capo("Frank Nitti", 40);
	Bureaucrat		godfather("Al Capone", 1);
	std::cout << indic << std::endl;
	std::cout << capo << std::endl;
	std::cout << godfather << std::endl;	
	std::cout << std::endl;
	std::cout << "\033[1;33m\t########## Basics ########## \033[0m" << std::endl;
	{
		try	{

			std::cout << "\033[1;37mForm creation\033[0m" << std::endl;

			Form	*Shrub = new ShrubberyCreationForm("happyTree");
			Form	*Robot = new RobotomyRequestForm("Dirk Gently");
			Form	*Pardon = new PresidentialPardonForm("Jim Carrey");
			std::cout << *Shrub << std::endl;
			std::cout << *Robot << std::endl;
			std::cout << *Pardon << std::endl;
			std::cout << std::endl;	

			std::cout << "\033[1;37mForm actions\033[0m" << std::endl;

			indic.signForm(*Shrub);
			indic.executeForm(*Shrub);
			capo.signForm(*Robot);
			capo.executeForm(*Robot);
			godfather.signForm(*Pardon);
			godfather.executeForm(*Pardon);
			std::cout << std::endl;	

			std::cout << "\033[1;37mForm deletion\033[0m" << std::endl;
			delete Shrub;
			delete Robot;
			delete Pardon;
		}
		catch(std::exception & error) {

			std::cout << error.what() << std::endl;
		}
	}
	std::cout << "\033[1;33m\t########## Test Exception ########## \033[0m" << std::endl;
	{
		try	{

			std::cout << "\033[1;37mForm creation\033[0m" << std::endl;

			Form	*Robot_2 = new RobotomyRequestForm("Arthur Dent");
			Form	*Pardon_2 = new PresidentialPardonForm("Daffy duck");
			std::cout << *Robot_2 << std::endl;
			std::cout << *Pardon_2 << std::endl;
			std::cout << std::endl;	

			std::cout << "\033[1;37mForm actions\033[0m" << std::endl;
			indic.signForm(*Robot_2);
			capo.signForm(*Robot_2);
			indic.executeForm(*Robot_2);
			godfather.executeForm(*Pardon_2);
			std::cout << std::endl;

			std::cout << "\033[1;37mForm deletion\033[0m" << std::endl;
			delete Robot_2;
			delete Pardon_2;
		}
		catch (std::exception & error) {

			std::cout << error.what() << std::endl;
		}

	}
	std::cout << std::endl;
	std::cout << "\033[1;37mBureaucrat deletion\033[0m" << std::endl;

	return 0;
}
