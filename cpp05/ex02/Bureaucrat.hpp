#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

# include "Form.hpp"
# include <exception>
# include <iostream>
# include <string>

class Form;

class Bureaucrat {

		const std::string	_name;
		unsigned int		_grade;

	public:

		Bureaucrat();
		Bureaucrat( std::string name, int grade );
		Bureaucrat( Bureaucrat const & src );
		~Bureaucrat();

		Bureaucrat &		operator=( Bureaucrat const & rhs );

		std::string			getName() const;
		unsigned int		getGrade() const;
		void				upGrade();
		void				downGrade();
		void				signForm( Form & form );
		void				executeForm( Form & form );

		class	GradeTooHighException: public std::exception {

			public:
				virtual const char* what() const throw() {
					return ("\033[0;35m>>> EXCEPTION BUREAUCRAT: \033[0mGrade too hight");
				}
		};
		class	GradeTooLowException: public std::exception {

			public:
				virtual const char* what() const throw() {
					return ("\033[0;35m>>> EXCEPTION BUREAUCRAT: \033[0mGrade too low");
				}
		};
		class	OupsSomethingWrong: public std::exception {

			public:
				virtual const char*	what() const throw() {
					return ("\033[0;35m>>> EXCEPTION BUREAUCRAT: \033[0mProblem exist between keyboard and chair");
				}
		};

};

std::ostream &			operator<<( std::ostream & o, Bureaucrat const & i );

#endif /* ****************************************************** BUREAUCRAT_H */