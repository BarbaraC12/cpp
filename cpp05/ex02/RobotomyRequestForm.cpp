#include "RobotomyRequestForm.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

RobotomyRequestForm::RobotomyRequestForm( void ) :Form("RobotomyRequest", 72, 45), _target("undefined") {

}

RobotomyRequestForm::RobotomyRequestForm( std::string target ) :Form("RobotomyRequest", 72, 45), _target(target) {

}

RobotomyRequestForm::RobotomyRequestForm( const RobotomyRequestForm & src ) :Form("RobotomyRequest", 72, 45), _target(src._target) {

}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

RobotomyRequestForm::~RobotomyRequestForm() {

}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

RobotomyRequestForm &				RobotomyRequestForm::operator=( RobotomyRequestForm const & rhs ) {
	if ( this != &rhs ) {

		this->_target = rhs._target;
		setSign(rhs.getIfSigned());
	}
	return *this;
}

/*
** --------------------------------- METHODS ----------------------------------
*/

void								RobotomyRequestForm::executeForm( void ) const {

	srand(time(NULL));
	int		i(rand());

	std::cout << "Wwwwwwrrrrrrrrrrrrwwwwwww Wwwwwwrrrrrrrrrrrrrrwwww " ;
	if ( i % 2 )
		std::cout << this->_target << " got robotomize" << std::endl;
	else
		std::cout << this->_target << " robotomy fail" << std::endl;

}

/* ************************************************************************** */