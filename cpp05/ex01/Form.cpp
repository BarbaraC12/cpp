#include "Form.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Form::Form( void ) :_name("no name"), _isSigned(false), _lvlToSign(150), _lvlToExecute(150) {

	std::cout << "\033[0;32m> \033[0mNew Default Form write." << std::endl;
}

Form::Form( std::string name, unsigned int lvlSign, unsigned int lvlExecute ) :_name(name), _isSigned(false), _lvlToSign(lvlSign), _lvlToExecute(lvlExecute) {

	if (_lvlToSign < 1 || _lvlToExecute < 1 )
		throw Form::GradeTooHighException();
	if (_lvlToSign > 150 || _lvlToExecute > 150)
		throw Form::GradeTooHighException();
	std::cout << "\033[0;32m> \033[0mNew Form write." << std::endl;
}

Form::Form( const Form & src ) : _name(src._name), _isSigned(src._isSigned), _lvlToSign(src._lvlToSign), _lvlToExecute(src._lvlToExecute)
{
	if (_lvlToSign < 1 || _lvlToExecute < 1 )
		throw Form::GradeTooHighException();
	if (_lvlToSign > 150 || _lvlToExecute > 150)
		throw Form::GradeTooHighException();
	std::cout << "\033[0;32m> \033[0mCopy constructor called\033[0m" << std::endl;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Form::~Form(){

	std::cout << "\033[0;31m< \033[0mForm burn\033[0m" << std::endl;
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Form &				Form::operator=( Form const & rhs )
{
	if ( this != &rhs )
		_isSigned = rhs.getIfSigned();
	return *this;
}

std::ostream &			operator<<( std::ostream & o, Form const & rhs ) {
	
	o << "\nFORM NAME  ----------  " << rhs.getName() << std::endl;
	o << "-> is signed:          ";
	if ( !rhs.getIfSigned() )
		o << "False" << std::endl;
	else
		o << "True" << std::endl;
	o << "-> grade to signed:    " << rhs.getLvlToSign() << std::endl;
	o << "-> grade to execute:   " << rhs.getLvlToExecute() << std::endl;
	return o;
}


/*
** --------------------------------- METHODS ----------------------------------
*/

void			Form::beSigned( Bureaucrat const & bureaucrat ) {

	if ( bureaucrat.getGrade() > this->_lvlToSign )
		throw Form::GradeTooLowException();
	if ( this->_isSigned == true )
		throw Form::AlreadySignedException();
	this->_isSigned = true;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

std::string		Form::getName( void ) const {

	return this->_name;
}

unsigned int	Form::getLvlToSign( void ) const {

	return this->_lvlToSign;
}

unsigned int	Form::getLvlToExecute( void ) const {

	return this->_lvlToExecute;
}

bool			Form::getIfSigned( void ) const {

	return this->_isSigned;
}

/* ************************************************************************** */