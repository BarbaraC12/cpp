#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

# include <iostream>
# include <exception>
# include <string>
# include "Form.hpp"

class Form;

class Bureaucrat {

	public:

		Bureaucrat();
		Bureaucrat( std::string name, int grade );
		Bureaucrat( Bureaucrat const & src );
		~Bureaucrat();

		Bureaucrat &		operator=( Bureaucrat const & rhs );

		std::string			getName() const;
		unsigned int		getGrade() const;
		void				upGrade();
		void				downGrade();
		void				signForm( Form& form);

		class	GradeTooHighException: public std::exception {

			public:
				virtual const char* what() const throw() {
					return ("\033[0;35m>>> EXCEPTION BUREAUCRAT: \033[0mGrade too hight");
				}
		};
		class	GradeTooLowException: public std::exception {

			public:
				virtual const char* what() const throw() {
					return ("\033[0;35m>>> EXCEPTION BUREAUCRAT: \033[0mGrade too low");
				}
		};
		class	OupsSomethingWrong: public std::exception {

			public:
				virtual const char*	what() const throw() {
					return ("\033[0;35m>>> EXCEPTION BUREAUCRAT: \033[0mProblem exist between keyboard and chair");
				}
		};

	private:

		const std::string	_name;
		unsigned int		_grade;
};

std::ostream &			operator<<( std::ostream & o, Bureaucrat const & i );

#endif /* ****************************************************** BUREAUCRAT_H */