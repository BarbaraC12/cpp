#ifndef FORM_HPP
# define FORM_HPP

# include <iostream>
# include <string>
# include "Bureaucrat.hpp"

class Bureaucrat;

class Form {

	std::string const	_name;
	bool				_isSigned;
	unsigned int const	_lvlToSign;
	unsigned int const	_lvlToExecute;

	public:

		Form( void );
		Form( std::string name, unsigned int lvlSign, unsigned int lvlExecute ) ;
		Form( Form const & src );
		~Form();

		Form &		operator=( Form const & rhs );
		
		std::string		getName( void ) const;
		unsigned int	getLvlToSign( void ) const;
		unsigned int	getLvlToExecute( void ) const;
		bool			getIfSigned( void ) const;
		void			beSigned( Bureaucrat const & bureaucrat  );
	
		class	GradeTooHighException: public std::exception {

			public:
				virtual const char* what() const throw() {
					return ("\033[0;35m>>> EXCEPTION FORM: \033[0mGrade too hight");
				}
		};
		class	GradeTooLowException: public std::exception {

			public:
				virtual const char* what() const throw() {
					return ("\033[0;35m>>> EXCEPTION FORM: \033[0mGrade too low");
				}
		};
		class	AlreadySignedException: public std::exception {

			public:
				virtual const char* what() const throw() {
					return ("\033[0;35m>>> EXCEPTION FORM: \033[0mForm is already signed");
				}
		};
		class OupsSomethingWrong: public std::exception {

			public:
				virtual const char*	what() const throw() {
					return ("\033[0;35m>>> EXCEPTION FORM: \033[0mProblem exist between keyboard and chair");
				}
		};

};

std::ostream &			operator<<( std::ostream & o, Form const & i );

#endif /* ************************************************************ FORM_H */