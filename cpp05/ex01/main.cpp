#include "Bureaucrat.hpp"

int		main()
{
	std::cout << "\033[1;33m\t########## Basics ########## \033[0m" << std::endl;
	{
		try	{

			Bureaucrat	Boss( "El kaïd", 1 );
			Bureaucrat	Stagiaire;
			Form		Name("Press red button", 15, 1);
			std::cout << Boss << std::endl;
			std::cout << Stagiaire << std::endl;
			std::cout << Name << std::endl;
			Stagiaire.signForm(Name);
			std::cout << Name << std::endl;
			Boss.signForm(Name);
			std::cout << Name << std::endl;
		}
		catch(std::exception & error) {

			std::cout << error.what() << std::endl;
		}
	}

	std::cout << "\033[1;33m\t########## Test down at lvl 150 ########## \033[0m" << std::endl;
	{
		try	{

			Bureaucrat	Stagiaire;
			Form		Name("Test", 149, 1);
			std::cout << Stagiaire << std::endl;
			std::cout << Name << std::endl;
			Stagiaire.signForm(Name);
			std::cout << Name << std::endl;
			Stagiaire.upGrade();
			std::cout << Stagiaire << std::endl;
			Stagiaire.signForm(Name);
			std::cout << Name << std::endl;
		}
		catch(std::exception & error) {

			std::cout << error.what() << std::endl;
		}
	}

	std::cout << "\033[1;33m\t########## Test Form = 0 ########## \033[0m" << std::endl;
	{	
		Bureaucrat	Stagiaire;
		try	{

			Form		Name("Test", 0, 148);
			throw Form::OupsSomethingWrong();
		}
		catch(std::exception & error) {

			std::cout << error.what() << std::endl;
		}
	}
	std::cout << "\033[1;33m\t########## Test Form = 155 ########## \033[0m" << std::endl;
	{	
		Bureaucrat	Stagiaire;
		try	{

			Form		Name("Test", 155, 148);
			throw Form::OupsSomethingWrong();

		}
		catch(std::exception & error) {

			std::cout << error.what() << std::endl;
		}
	}
}