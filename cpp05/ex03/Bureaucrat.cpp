#include "Bureaucrat.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Bureaucrat::Bureaucrat( void ) :_name("Stagiaire"), _grade(150) {

	std::cout << "\033[0;32m> \033[0mNew Bureaucrat called." << std::endl;
}

Bureaucrat::Bureaucrat( std::string name, int grade ) :_name(name) {

	if ( grade > 150 )
		throw Bureaucrat::GradeTooLowException();
	else if ( grade < 1 )
		throw Bureaucrat::GradeTooHighException();
	this->_grade = grade;
	std::cout << "\033[0;32m> \033[0mNew Bureaucrat " << this->_name << " called." << std::endl;
}

Bureaucrat::Bureaucrat( const Bureaucrat & src ) {
	
	*this = src;
	std::cout << "Copy Bureaucrat called" << std::endl;
}

/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Bureaucrat::~Bureaucrat() {

	std::cout << "\033[0;31m< \033[0mBureaucrat destructor called." << std::endl;
}

/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Bureaucrat &				Bureaucrat::operator=( Bureaucrat const & rhs ) {
	if ( this != &rhs )
	{
		this->_grade = rhs.getGrade();
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, Bureaucrat const & i ) {

	o << i.getName() << ", bureaucrat grade " << i.getGrade();
	return o;
}

/*
** --------------------------------- METHODS ----------------------------------
*/

void				Bureaucrat::upGrade() {

	try {

		std::cout << "Try to promote." << std::endl;
		if ( this->_grade > 1 ) {
			this->_grade -= 1;
			std::cout << "\033[0;34m>> \033[0m" << this->getName() << " has been promoted. NEW RANK: " << this->getGrade() << std::endl;
		}
		else {
			throw Bureaucrat::GradeTooHighException();
		}
	}
	catch(std::exception & error) {

		std::cout << error.what() << std::endl;
	}
}

void				Bureaucrat::downGrade() {

	try {

		std::cout << "Try to downgrade." << std::endl;
		if ( this->_grade < 150 ) {
			this->_grade += 1;
			std::cout << "\033[0;34m<< \033[0m" << this->getName() << " has been downgrade. NEW RANK: " << this->getGrade() << std::endl;
		}
		else {
			throw Bureaucrat::GradeTooLowException();
		}
	}
	catch(std::exception & error) {

		std::cout << error.what() << std::endl;
	}
}

void				Bureaucrat::signForm( Form& form) {

	std::cout << "Try to sign form." << std::endl;
	try {

		form.beSigned(*this);
		std::cout << "\033[0;34m>> \033[0m" << *this << " signed " << form.getName() << std::endl;
	}
	catch(std::exception & error) {

		std::cout << *this << " cannot sign " << form.getName() << std::endl << error.what() << std::endl;
	}
}

void				Bureaucrat::executeForm( Form& form ) {

	std::cout << "Try to execute form." << std::endl;
	try {

		form.execute(*this);
		std::cout << "\033[0;34m>> \033[0m" << *this << " executed " << form.getName() << std::endl;
	}
	catch (std::exception & error) {

		std::cerr << *this << " cannot execute " << form.getName() << std::endl << error.what() << std::endl;
	}
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

std::string						Bureaucrat::getName() const {

	return this->_name;
}

unsigned int					Bureaucrat::getGrade() const {

	return this->_grade;
}


/* ************************************************************************** */