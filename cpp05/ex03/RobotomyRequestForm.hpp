#ifndef ROBOTOMYREQUESTFORM_HPP
# define ROBOTOMYREQUESTFORM_HPP

# include "Form.hpp"
# include <cstdlib>
# include <ctime>

class RobotomyRequestForm : public Form {

		std::string					_target;

	public:

		RobotomyRequestForm( );
		RobotomyRequestForm( std::string target );
		RobotomyRequestForm( RobotomyRequestForm const & src );
		~RobotomyRequestForm();

		RobotomyRequestForm &		operator=( RobotomyRequestForm const & rhs );

		void						executeForm( void ) const;
		Form*						dupForm( std::string target );


};

#endif /* ********************************************* ROBOTOMYREQUESTFORM_H */