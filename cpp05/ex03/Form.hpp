#ifndef FORM_HPP
# define FORM_HPP

# include "Bureaucrat.hpp"
# include <iostream>
# include <string>

class Bureaucrat;

class Form {

	std::string const	_name;
	bool				_isSigned;
	unsigned int const	_lvlToSign;
	unsigned int const	_lvlToExecute;

	public:

		Form( void );
		Form( std::string name, unsigned int lvlSign, unsigned int lvlExecute ) ;
		Form( Form const & src );
		virtual ~Form();

		Form &		operator=( Form const & rhs );
		
		std::string		getName( void ) const;
		unsigned int	getLvlToSign( void ) const;
		unsigned int	getLvlToExecute( void ) const;
		bool			getIfSigned( void ) const;
		void			setSign( bool sign );
		void			beSigned( Bureaucrat const & bureaucrat ); 
		void			execute( Bureaucrat const & executor );
		virtual void	executeForm( void ) const = 0;
		virtual Form*	dupForm( std::string target ) = 0;

		class	GradeTooHighException: public std::exception {

			public:
				virtual const char* what() const throw() {
					return ("\033[0;35m>>> EXCEPTION FORM: \033[0mGrade too hight");
				}
		};
		class	GradeTooLowException: public std::exception {

			public:
				virtual const char* what() const throw() {
					return ("\033[0;35m>>> EXCEPTION FORM: \033[0mGrade too low");
				}
		};
		class	AlreadySignedException: public std::exception {

			public:
				virtual const char* what() const throw() {
					return ("\033[0;35m>>> EXCEPTION FORM: \033[0mForm is already signed");
				}
		};
		class	NotSignedException: public std::exception {

			public:
				virtual const char* what() const throw() {
					return ("\033[0;35m>>> EXCEPTION FORM: \033[0mForm is not signed");
				}
		};
		class	OupsSomethingWrong: public std::exception {

			public:
				virtual const char*	what() const throw() {
					return ("\033[0;35m>>> EXCEPTION FORM: \033[0mProblem exist between keyboard and chair");
				}
		};

};

std::ostream &			operator<<( std::ostream & o, Form const & i );

#endif /* ************************************************************ FORM_H */