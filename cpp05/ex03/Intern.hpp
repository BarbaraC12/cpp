#ifndef INTERN_HPP
# define INTERN_HPP

#include "PresidentialPardonForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "Form.hpp"
# include <iostream>
# include <string>

class Intern {

		Form*	_possibleForm[3];

	public:

		Intern();
		Intern( Intern const & src );
		~Intern();

		Intern &		operator=( Intern const & rhs );

		Form*			makeForm( std::string name, std::string target );

		class InvalidFormNameException: public std::exception {

			public:
				virtual const char *	what() const throw() {
					return "\033[0;35m>>> EXCEPTION INTERN: \033[0mForm with this name not found";
				}
		};

};

std::ostream &			operator<<( std::ostream & o, Intern const & i );

#endif /* ********************************************************** INTERN_H */