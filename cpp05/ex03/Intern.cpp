#include "Intern.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Intern::Intern() {

	_possibleForm[0] = new ShrubberyCreationForm();
	_possibleForm[1] = new RobotomyRequestForm();
	_possibleForm[2] = new PresidentialPardonForm();

}

Intern::Intern( const Intern & src ) {

	for (int i(0); i < 3 ; i++ )
		this->_possibleForm[i] = src._possibleForm[i];
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Intern::~Intern() {

	for (int i = 0; i < 3; i++) {

		delete _possibleForm[i];
	}
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Intern &				Intern::operator=( Intern const & rhs ) {

	if (this != &rhs) {

		for (int i = 0; i < 3; i++) {

			delete _possibleForm[i];
			this->_possibleForm[i] = rhs._possibleForm[i];
		}
	}
	return *this;
}


/*
** --------------------------------- METHODS ----------------------------------
*/

Form *				Intern::makeForm( std::string name, std::string target)
{
	for (int i(0); i < 3; i++) {

		if (name == this->_possibleForm[i]->getName()) {
			Form * clone = this->_possibleForm[i]->dupForm(target);
			std::cout << "Intern creates " << *clone << std::endl;
			return clone;
		}
	}
	throw InvalidFormNameException();
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/


/* ************************************************************************** */