#ifndef SHRUBBERYCREATIONFORM_HPP
# define SHRUBBERYCREATIONFORM_HPP

# include "Form.hpp"
# include <fstream>

class ShrubberyCreationForm : public Form {

		std::string					_target;

	public:

		ShrubberyCreationForm( );
		ShrubberyCreationForm( std::string target );
		ShrubberyCreationForm( ShrubberyCreationForm const & src );
		~ShrubberyCreationForm();

		ShrubberyCreationForm &		operator=( ShrubberyCreationForm const & rhs );

		void						executeForm( void ) const;
		Form*						dupForm( std::string target );

};

#endif /* ******************************************* SHRUBBERYCREATIONFORM_H */