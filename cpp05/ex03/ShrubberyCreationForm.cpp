#include "ShrubberyCreationForm.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

ShrubberyCreationForm::ShrubberyCreationForm( void ) :Form("ShrubberyCreation", 145, 137), _target("undefined") {
	
}

ShrubberyCreationForm::ShrubberyCreationForm( std::string target ) :Form("ShrubberyCreation", 145, 137), _target(target) {
	
}

ShrubberyCreationForm::ShrubberyCreationForm( const ShrubberyCreationForm & src ) :Form(src), _target(src._target) {

}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

ShrubberyCreationForm::~ShrubberyCreationForm() {
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

ShrubberyCreationForm &				ShrubberyCreationForm::operator=( const ShrubberyCreationForm & rhs )
{
	if ( this != &rhs ) {

		this->_target = rhs._target;
		setSign(rhs.getIfSigned());
	}
	return *this;
}

/*
** --------------------------------- METHODS ----------------------------------
*/

void								ShrubberyCreationForm::executeForm( void ) const {

	std::string		name( this->_target + "_shrubbery" );
	std::ofstream	out( name.c_str() );

	out << "                                      \n";
	out << "	       ,.,                        \n";
	out << "      MMMM_    ,..,                   \n";
	out << "        *_ *__*MMMMM          ,...,,  \n";
	out << " ,..., __.* --*    ,.,     _-*MMMMMMM \n";
	out << "MMMMMM*___ *_._   MMM*_.** _ ******   \n";
	out << " *****    ** , \\_.   *_. .*           \n";
	out << "        ,., _*__ \\__./ .*             \n";
	out << "       MMMMM_*  *_    ./              \n";
	out << "        ''''      (    )              \n";
	out << " ._______________.-'____*---._.       \n";
	out << "  \\                          /        \n";
	out << "   \\________________________/         \n";
	out << "   (_)                    (_)         \n";
	out << "                                      " << std::endl;

}

Form *								ShrubberyCreationForm::dupForm( std::string target )
{
	Form *	clone = new ShrubberyCreationForm(target);
	return clone;
}


/* ************************************************************************** */