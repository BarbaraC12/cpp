#ifndef STATICCAST_HPP
# define STATICCAST_HPP

# include <iostream>
# include <iomanip>
# include <cerrno>
# include <cfloat>
# include <climits>
# include <cstdio>
# include <cstdlib>
# include <cstring>
# include <string>
# include <cmath>

# define INDISPLAY	"Non displayable"
# define IMPOSIBLE	"impossible"


bool	cppIsspace ( char const c );
bool	cppIsdigit( char const c );

bool	isPrint( char const *input);
bool	isDigit( char const *input);
bool	isFloat( char const *input);
bool	isDouble( char const *input);

void	convFromPrintable( const char *input);
void	convFromDigit( const char *input);
void	convFromFloat( const char *input);
void	convFromDouble( const char *input);
void	nonConvertible( const char *input);
void	nonPrintable( void );

typedef void				( *t_castor )( char const *str );
typedef bool				( *t_shaker )( char const *str );
typedef struct s_jodufour	t_convert;

struct s_jodufour {
	
	t_shaker const			type;
	t_castor const			convertFrom;
};


class StaticCast {

		char const 					*_str;
		static t_convert const		_lookUp[]; 

	public:

		StaticCast( char * arg );
		~StaticCast();


	// private:

	

};


#endif /* ****************************************************** STATICCAST_H */