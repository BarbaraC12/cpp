#include "StaticCast.hpp"

int		main(int argc, char **argv)
{
	if (argc == 2) {

		StaticCast			toCast( argv[1] );
		
		return 0;
	}
	std::cout << "Error: Usage: Wrong number of argument" << std::endl;
	std::cout << "\t./scalar <toConvert> (char-int-float-double)" << std::endl;
	return 1;
}