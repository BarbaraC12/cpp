#include "StaticCast.hpp"

void	convFromPrintable( const char *input ) {
	
	int		a( static_cast<int>(*input) );

	std::cout << std::fixed << std::setprecision(1);
	std::cout << "Char   : '" << input << "'" <<std::endl;
	std::cout << "Int    : " << a << std::endl;
	std::cout << "Float  : " << static_cast<float>(a) << "f"<< std::endl;
	std::cout << "Double : " << static_cast<double>(a) << std::endl;
}

void	convFromDigit( const char *input ) {

	long	a(strtol(input, NULL, 10));

	std::cout << std::fixed << std::setprecision(1);
	if ( errno ||  a < INT_MIN || a > INT_MAX )
		nonPrintable();
	else {
		std::cout << "Char   : ";
		if ( a < CHAR_MIN || a > CHAR_MAX )
			std::cout << IMPOSIBLE << std::endl;
		else if ( std::isprint( a ))
			std::cout << "'" << static_cast<char>(a) << "'" << std::endl;
		else 
			std::cout << INDISPLAY << std::endl;
		std::cout << "Int    : " << a << std::endl;
		std::cout << "Float  : " << static_cast<float>(a) << "f"<< std::endl;
		std::cout << "Double : " << static_cast<double>(a) << std::endl;

	}
}

void	convFromFloat( const char *input ) {

	float		a(strtof(input, NULL)) ;
	
	std::cout << std::fixed << std::setprecision(1);
	if ( errno )
		nonPrintable();
	else {
		std::cout << "Char   : ";
		if ( round(a) < CHAR_MIN 
		|| round(a) > CHAR_MAX
		|| isnanf(a) )
			std::cout << IMPOSIBLE << std::endl;
		else if ( std::isprint( round(a) ))
			std::cout << "'" << static_cast<char>(a) << "'" << std::endl;
		else 
			std::cout << INDISPLAY << std::endl;
		std::cout << "Int    : ";
		if ( round(a) < INT_MIN
			|| round(a) > INT_MAX
			|| isnanf(a) )
			std::cout << IMPOSIBLE << std::endl;
		else
			std::cout << static_cast<int>(a) << std::endl;
		std::cout << "Float  : " << a << "f" << std::endl;
		std::cout << "Double : " << static_cast<double>(a) << std::endl;
	}
}

void	convFromDouble( const char *input ) {

	double		a(strtod(input, NULL));

	std::cout << std::fixed << std::setprecision(1);
	if ( errno )
		nonPrintable();
	else {
		std::cout << "Char   : ";
		if ( round(a) < CHAR_MIN
			|| round(a) > CHAR_MAX
			|| isnan(a) )
			std::cout << IMPOSIBLE << std::endl;
		else if ( std::isprint( round(a) ))
			std::cout << "'" << static_cast<char>(a) << "'" << std::endl;
		else 
			std::cout << INDISPLAY << std::endl;
		std::cout << "Int    : ";
		if ( round(a) < INT_MIN
			|| round(a) > INT_MAX
			|| isnan(a) )
			std::cout << IMPOSIBLE << std::endl;
		else
			std::cout << static_cast<int>(a) << std::endl;
		std::cout << std::fixed;
		if ( round(a) < -FLT_MAX
			&& round(a) > FLT_MAX )
			std::cout << "Float  : " << IMPOSIBLE << std::endl;
		else
			std::cout << "Float  : " << static_cast<float>(a) << "f"<< std::endl;
		std::cout << "Double : " << a << std::endl;
	}
}

void	nonConvertible( const char *input) {

	std::cout << "Error: Usage: Bad argument: '" << input << "' is non-convertible"<< std::endl;
}

void	nonPrintable( void ) {

	std::cout 	<< "Char   : " << IMPOSIBLE << std::endl
				<< "Int    : " << IMPOSIBLE << std::endl
				<< "Float  : " << IMPOSIBLE << std::endl
				<< "Double : " << IMPOSIBLE << std::endl;
}

