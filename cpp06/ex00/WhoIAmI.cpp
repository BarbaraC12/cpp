#include "StaticCast.hpp"

bool	cppIsspace ( const char c ){

	return std::isspace( static_cast<unsigned char>(c) );
}

bool	cppIsdigit( const char c ) {

    return std::isdigit(static_cast<unsigned char>(c));
}

bool	isPrint( const char *input) {

	if ( !input
		|| *++input )
		return false;
	return true;
}

bool	isDigit( const char *input) {

	if ( !*input )
		return false;
	while ( cppIsspace(*input) )
		++input;
	if ( *input == '+' || *input == '-' )
		++input;
	if ( !cppIsdigit(*input) )
		return false;
	while ( cppIsdigit(*input) )
		++input;
	if ( *input )
		return false;
	return true;
}

bool	isFloat( const char *input) {

	if (!*input)
		return false;
	if ( std::strcmp( input, "-inff" ) == 0
		|| std::strcmp( input, "+inff" ) == 0
		|| std::strcmp( input, "inff" ) == 0
		|| std::strcmp( input, "nanf" ) == 0 )
		return true;
	while (cppIsspace(*input))
		++input;
	if (*input == '+' || *input == '-')
		++input;
	while (cppIsdigit(*input))
		++input;
	if ( !*input )
		return false;
	if (*input == '.')
		++input;
	while (cppIsdigit(*input))
		++input;
	if ( *input != 'f' )
		return false;
	if ( *++input )
		return false;
	return true;
}

bool	isDouble( const char *input) {

	if (!*input)
		return false;
	if ( std::strcmp( input, "-inf" ) == 0
		|| std::strcmp( input, "+inf" ) == 0
		|| std::strcmp( input, "inf" ) == 0
		|| std::strcmp( input, "nan" ) == 0 )
		return true;
	while (cppIsspace(*input))
		++input;
	if (*input == '+' || *input == '-')
		++input;
	while (cppIsdigit(*input))
		++input;
	if ( !*input )
		return false;
	if (*input == '.')
		++input;
	while (cppIsdigit(*input))
		++input;
	if (*input)
		return false;
	return true;
}
