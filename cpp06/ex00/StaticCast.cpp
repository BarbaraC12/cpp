#include "StaticCast.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

StaticCast::StaticCast( char * arg ) :_str( (const char *)arg ){

	std:: cout << std::setprecision(1) << std::fixed;
	int		i(0);

	for (; ( StaticCast::_lookUp[i].type && !StaticCast::_lookUp[i].type( _str ) ) ; i++ );
	StaticCast::_lookUp[i].convertFrom( _str );
}

/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

StaticCast::~StaticCast() { }

/*
** -------------------------------- ATTR PRIVATE ------------------------------
*/

t_convert const		StaticCast::_lookUp[] = {

	{isDigit, convFromDigit},
	{isPrint, convFromPrintable},
	{isFloat, convFromFloat},
	{isDouble, convFromDouble},
	{NULL, nonConvertible}
};



/* ************************************************************************** */