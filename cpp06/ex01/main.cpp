#include "Data.hpp"

int			main()
{
	Data d;
	Data *ptr;
	uintptr_t uiptr;

	d.i = 42;
	{
		uiptr = serialize(&d);
		ptr = deserialize(uiptr);
		std::cout << std::endl;
		std::cout << "------- TEST INTEGER -------" << std::endl;
		std::cout << "Data         : " << d.i << " - " << &d.i << std::endl;
		std::cout << "Serialize    : " << uiptr << std::endl;
		std::cout << "Deserialize  : " << ptr->i << " - " << &ptr->i << std::endl;
		std::cout << std::endl;
	}
	{
		d.f = d.i;
		uiptr = serialize(&d);
		ptr = deserialize(uiptr);
		std::cout << "-------  TEST FLOAT  -------" << std::endl;
		
		std::cout << std::fixed << std::setprecision(1);
		std::cout << "Data         : " << d.f << " - " << &d.f << std::endl;
		std::cout << "Serialize    : " << uiptr << std::endl;
		std::cout << "Deserialize  : " << ptr->f << " - " << &ptr->f << std::endl;
		std::cout << std::endl;
	}
	{
		d.ptr = &d;
		uiptr = serialize(&d);
		ptr = deserialize(uiptr);
		std::cout << "-------   TEST PTR   -------" << std::endl;

		std::cout << "Data         : " << d.ptr << " - " << &d.ptr << std::endl;
		std::cout << "Serialize    : " << uiptr << std::endl;
		std::cout << "Deserialize  : " << ptr->ptr << " - " << &ptr->ptr << std::endl;
	}

	return (0);
}