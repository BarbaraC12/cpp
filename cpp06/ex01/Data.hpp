#ifndef DATA_HPP
# define DATA_HPP

# include <iostream>
# include <stdint.h>
# include <iomanip>

struct Data {

	public:

		int		i;
		void*	ptr;
		float	f;
};

uintptr_t		serialize( Data *ptr );
Data	*		deserialize( uintptr_t raw );

#endif