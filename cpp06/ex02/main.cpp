#include "Base.hpp"

Base *	generate( void ) {

	int i(rand() % 3);

	if ( i == 0 ) {
		std::cout << "\033[1;35mA\033[0m" << std::endl;
		return new A;
	}
	else if ( i == 1 ) {
		std::cout << "\033[1;35mB\033[0m" << std::endl;
		return new B;
	}
	else {
		std::cout << "\033[1;35mC\033[0m" << std::endl;
		return new C;
	}
}

void		identify(Base * p) {

	if (dynamic_cast<A *>(p))
		std::cout << "\033[1;34mA\033[0m";
	else if (dynamic_cast<B *>(p))
		std::cout << "\033[1;34mB\033[0m";
	else if (dynamic_cast<C *>(p))
		std::cout << "\033[1;34mC\033[0m";
	else
		std::cout << "Damn! Looks like there is an error between the keyboard and the chair";
}

void		identify(Base & p) {

	try {
		(void)dynamic_cast<A &>(p);
		std::cout << "\033[1;34mA\033[0m";
		return ;
	}
	catch (std::exception & error) {}
	try {
		(void)dynamic_cast<B &>(p);
		std::cout << "\033[1;34mB\033[0m";
		return ;
	}
	catch (std::exception & error) {}
	try {
		(void)dynamic_cast<C &>(p);
		std::cout << "\033[1;34mC\033[0m";
		return ;
	}
	catch (std::exception & error) {}
	std::cout << "Damn! Looks like there is an error between the keyboard and the chair";
}

int		main() {

	srand(time(NULL));
	for (int i(0); i < 10; i++)
	{
		std::cout	<< "\033[1;33m --------------- CALLED ----- ";
		Base *	identity = generate();
		std::cout	<< "void identify( Base * p )   : ";
		identify(identity);
		std::cout	<< std::endl;
		std::cout	<< "void identify( Base & p )   : ";
		identify(*identity);
		std::cout	<< std::endl;
		delete identity;
	}
	return (0);
}
