#include <iostream>
#include <clocale>

int main( int ac, char **av ) {
	
	std::string	buff;

	for (int i = 0; i < ac; i++) {
		if ( ac == 1 )
			buff = "* loud and unbearable feedback noice *";
		else {
			if ( i == 0 )
				i++;
			buff = av[i];
		}
		for ( std::string::iterator p = buff.begin(); buff.end() != p; ++p )
			*p = std::toupper(*p);
		std::cout << "\033[1;34m" << buff << "\033[0m"; 
	}
	std::cout << std::endl;

	return 0;
}