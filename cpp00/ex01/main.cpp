#include <iostream>
#include <cstring>
#include <ctype.h>
#include <clocale>
#include "PhonebookContact.hpp"

void	printHeader( void ) {

	std::cout << "\033[2J\033[1;1H";
	std::cout << "* ********************************************* *" << std::endl;
	std::cout << "* \033[0;36m           British Business Annuaire         \033[0m *" << std::endl;
	std::cout << "* ********************************************* *" << std::endl;
	std::cout << std::endl << "Hi welcome to this BBA !" << std::endl;
	std::cout << "To use this annuaire this is simple, you have 3 actions possibles." << std::endl;
}

void    printMenu( int c ) {

	std::cout << std::endl << "#######################################################" << std::endl;
	std::cout << std::endl << "This is the possible action list:" << std::endl;
	std::cout << "- \033[0;33mADD\033[0m    : add new contact details." << std::endl;
	std::cout << "- \033[0;33mSEARCH\033[0m : util to find your coworker reference." << std::endl;
	std::cout << "- \033[0;33mEXIT\033[0m   : for close annuaire." << std::endl;
	if ( c > 0 )
		std::cout << "Registred contact: " << c << std::endl;
	std::cout << std::endl;
}

int main( void ) {
	
	int		id( 0 );
	int		register_contact( 0 );
	Phonebook	book;
	std::string	buff;

	printHeader();
	while ( 1 )
	{
		buff.clear( );
		if ( std::cin.eof() )
			break ;
		printMenu( register_contact );
		std::cout << "Your instruction: ";
		getline( std::cin, buff );
		std::cout << std::endl;
		for ( std::string::iterator p = buff.begin(); buff.end() != p; ++p )
				*p = toupper(*p);
		if ( buff == "ADD")
		{
			if ( id <= CONTACT_MAX)
			{
				if ( register_contact < CONTACT_MAX)
					register_contact++;
				if ( id == CONTACT_MAX)
					id %= CONTACT_MAX ;	
				book.add( id++ );
				std::cout << "\033[2J\033[1;1H";
			}
			else {
				std::cout << "\033[2J\033[1;1H";
				std::cout << "An error has occurred: Try again." << std::endl;
			}
		}
		else if ( buff == "SEARCH" )
		{
			std::cout << "\033[2J\033[1;1H";
			book.search( register_contact );
		}
		else if ( buff == "EXIT" )
			break ;
		else if ( buff.length( ) > 1 )
		{
			std::cout << "\033[2J\033[1;1H";
			std::cout << "Warning input don't match to possible actions" << std::endl;
		}

	}
	std::cout << "Goodbye " << std::endl;
	return 0;
}