#include <iostream>
#include "PhonebookContact.hpp"

Contact::Contact( ) {
    return ;
}

Contact::~Contact( ) {
    return ;
}

void	putContact( std::string parameter) {

	if ( parameter.length() <= 10 )
	{
		std::cout.width(10);
		std::cout << parameter;
	}
	else
	{
		int	i ( 0 );
		while ( i < 9 )
		{
			std::cout << parameter[i];
			i++;
		}
		std::cout << ".";
	}
}

std::string		Contact::getFirstname( void ) const {
	
	return this->_firstName;
}

std::string		Contact::getLastname( void ) const {
	
	return this->_lastName;
}

std::string		Contact::getNickname( void ) const {
	
	return this->_nickname;
}

std::string		Contact::getPhoneNumber( void ) const {
	
	return this->_phoneNumber;
}

std::string		Contact::getDarkestSecret( void ) const {
	
	return this->_darkSecret;
}

void			Contact::setFirstname( std::string str) {
	
	this->_firstName = str;
}

void			Contact::setLastname( std::string str) {
	
	this->_lastName = str;
}

void			Contact::setNickname( std::string str) {
	
	this->_nickname = str;
}

void			Contact::setPhoneNumber( std::string str) {
	
	this->_phoneNumber = str;
}

void			Contact::setDarkestSecret( std::string str) {
	
	this->_darkSecret = str;
}

void	Contact::print( ) {

	std::cout << "|  ";
	std::cout << this->index;
	std::cout << " | ";
	putContact( getFirstname( ) );
	std::cout << " | ";
	putContact( getLastname( ) );
	std::cout << " | ";
	putContact( getNickname( ) );
	std::cout << " |" << std::endl;
}

void	Contact::getAttributes( ) {

	std::cout << " First Name: " << getFirstname() << std::endl;
	std::cout << " Last Name: " << getLastname() << std::endl;
	std::cout << " Nickname: " << getNickname() << std::endl;
	std::cout << " Phone Number: " << getPhoneNumber() << std::endl;
	std::cout << " Darkest Secret: " << getDarkestSecret() << std::endl;
}
