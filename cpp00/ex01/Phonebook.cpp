#include <iostream>
#include <cstdio>
#include <string>
#include "PhonebookContact.hpp"

Phonebook::Phonebook( ) {
    return ;
}

Phonebook::~Phonebook( ) {
    return ;
}


std::string	getParameter( const char *parameter, int verif ) {

	std::string	buff;
	long int	pNumber;
	bool		valid ( 0 );

	while ( !valid )
	{
		buff.clear( );
		if ( std::cin.eof() )
			break ;
		std::cout << parameter << " (minimum " << verif + 1 << " caracters): ";
		getline( std::cin, buff );
		if ( ( verif == 2 
			&& ( (int)buff.length() > verif && (int)buff.length() < 12 ) 
			&& sscanf(buff.c_str(), "%ld", &pNumber) == 1 ) 
			|| ( verif != 2 && (int)buff.length() > verif ) )
			valid = 1;
		else
			std::cout << "Invalid input." << std::endl;
	}
	return buff;
}

int Phonebook::add( int c ) {


	int id = c + 1;
	std::string lastName = getParameter( "Last name", 1 );
	std::string firstName = getParameter( "First name", 1 );
	std::string nickname = getParameter( "Nickname", 1 );
	std::string phoneNumber = getParameter( "Phone number", 2 );
	std::string darkestSecret = getParameter( "Darkest secret", 4 );

	this->contact[c].index = id;
	this->contact[c].setLastname( lastName );
	this->contact[c].setFirstname( firstName );
	this->contact[c].setNickname( nickname );
	this->contact[c].setPhoneNumber( phoneNumber );
	this->contact[c].setDarkestSecret( darkestSecret );
	return 1;
}

int Phonebook::search( int num ) {

	std::string	buff;
	int			i;;

	while ( 1 )
	{
		if ( num > 0)
		{
			int		id ( 0 );

			std::cout << "\033[2J\033[1;1H";
			if ( std::cin.eof() )
				break ;
			std::cout << "| ID | Firstname  |  Lastname  |  Nickname  |" << std::endl;
			std::cout << "|----|------------|------------|------------|" << std::endl;
			while ( id < num )
			{
				this->contact[id].print();
				id++;
			}
			std::cout << std::endl << "Enter the ID of the contact you want to watch ?  > ";
			getline( std::cin, buff );
			if(sscanf(buff.c_str(), "%d", &i) == 1)
			{
				if (i > 0 && i <= num)
				{
					this->contact[i - 1].getAttributes();
					break ;
				}
			}
		}
		std::cout << "No entry" << std::endl;
		break ;
	}
	return 1;
}