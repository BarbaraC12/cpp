#ifndef PHONEBOOKCONTACT_H
# define PHONEBOOKCONTACT_H

# define	CONTACT_MAX	8

class Contact {

	private:
		std::string		_firstName;
		std::string		_lastName;
		std::string		_nickname;
		std::string		_phoneNumber;
		std::string		_darkSecret;
	
	public:
		Contact( void );
		~Contact( void );

		int					index;
		void				print( );
		void				getAttributes( );

		std::string		getFirstname( void ) const;
		std::string		getLastname( void ) const;
		std::string		getNickname( void ) const;
		std::string		getPhoneNumber( void ) const;
		std::string		getDarkestSecret( void ) const ;

		void			setFirstname( std::string str);
		void			setLastname( std::string str);
		void			setNickname( std::string str);
		void			setPhoneNumber( std::string str);
		void			setDarkestSecret( std::string str);
};

class Phonebook {

public:
    Contact			contact[CONTACT_MAX];
	int				id;

    Phonebook( void );
    ~Phonebook( void );

	int				add( int c );
	int				search( int c );
};

#endif