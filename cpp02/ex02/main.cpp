#include "Fixed.hpp"
#include <iostream>

int main( void ) {

    Fixed a;
    Fixed const b( Fixed( 5.05f ) * Fixed( 2 ) );

    std::cout << "\033[1;33mDecrement, pre-decrement: \033[0m" << std::endl;
    std::cout << "\033[1;37ma = \033[0m" << a << std::endl;
    std::cout << "\033[1;37m--a = \033[0m" << --a << std::endl;
    std::cout << "\033[1;37ma = \033[0m" << a << std::endl;
    std::cout << "\033[1;37ma-- = \033[0m" << a-- << std::endl;
    std::cout << "\033[1;37ma = \033[0m" << a << std::endl;
    
    a = 0;
    std::cout << std::endl << "\033[1;33mIncrement, pre-increment: \033[0m" << std::endl;
    std::cout << "\033[1;37ma = \033[0m" << a << std::endl;
    std::cout << "\033[1;37m++a = \033[0m" << ++a << std::endl;
    std::cout << "\033[1;37ma = \033[0m" << a << std::endl;
    std::cout << "\033[1;37ma++ = \033[0m" << a++ << std::endl;
    std::cout << "\033[1;37ma = \033[0m" << a << std::endl;

    std::cout << std::endl << "\033[1;33mMin / Max 1: \033[0m" << std::endl;
    std::cout << "\033[1;37ma = \033[0m" << a << " \033[1;37mand b =\033[0m " << b << std::endl;
    std::cout << "\033[1;37mmax = \033[0m" << Fixed::max( a, b ) << std::endl;
    std::cout << "\033[1;37mmin = \033[0m" << Fixed::min( a, b ) << std::endl;

    std::cout << std::endl << "\033[1;33mMin / Max 2: \033[0m" << std::endl;
    a = 42.02f;
    std::cout << "\033[1;37ma = \033[0m" << a << " \033[1;37mand b =\033[0m " << b << std::endl;
    std::cout << "\033[1;37mmax = \033[0m" << Fixed::max( a, b ) << std::endl;
    std::cout << "\033[1;37mmin = \033[0m" << Fixed::min( a, b ) << std::endl;

    std::cout << std::endl << "\033[1;33mArithmetique Operator: \033[0m" << std::endl;
    std::cout << "\033[1;37ma = \033[0m" << a << std::endl;
    a = a - 12;
    std::cout << "\033[1;37ma - 12 = \033[0m" << a << std::endl;
    a = a * 2;
    std::cout << "\033[1;37ma * 2 = \033[0m" << a << std::endl;
    a = a / 3;
    std::cout << "\033[1;37ma / 3 = \033[0m" << a << std::endl;
    a = a + 22;
    std::cout << "\033[1;37ma + 22 = \033[0m" << a << std::endl;
    
    return 0;
}
