#include <cmath>
#include <iostream>
#include "Fixed.hpp"

Fixed::Fixed( void ) :_number( 0 ) {

}

Fixed::Fixed( Fixed const & rhs ) {

    *this = rhs;
}

Fixed::Fixed( const int iraw ) {

    this->_number = (iraw << this->_bits);
}

Fixed::Fixed( const float fraw ) {

    this->_number = roundf(fraw * (1 << this->_bits));
}

Fixed::~Fixed() {

}

Fixed& Fixed::operator=( Fixed const & rhs ) {

    this->_number = rhs.getRawBits();
    return *this ;
}

int		Fixed::getRawBits( void ) const {

	return (this->_number);
}

void	Fixed::setRawBits( int const raw ) {

	this->_number = raw;
}

int		Fixed::toInt( void ) const {

	return ( this->_number >> this->_bits );
}

float	Fixed::toFloat( void ) const {

	return ( (float)this->_number / ( 1 << this->_bits) );
}

Fixed const &	Fixed::min(Fixed & a, Fixed & b) {

	return ( (a < b ? a : b) );
}

Fixed const &	Fixed::max(Fixed & a, Fixed & b) {

	return ( (a > b ? a : b) );
}

Fixed const	&	Fixed::min(const Fixed & a, const Fixed & b) {

	return ( (a < b ? a : b) );
}

Fixed const	&	Fixed::max(const Fixed & a, const Fixed & b) {

	return ( (a > b ? a : b) );
}

Fixed Fixed::operator+( Fixed const & rhs )  {

	return ( Fixed( this->toFloat() + rhs.toFloat()) );
}

Fixed Fixed::operator-( Fixed const & rhs )  {

	return ( Fixed( this->toFloat() - rhs.toFloat()) );
}

Fixed Fixed::operator*( Fixed const & rhs )  {

	return ( Fixed( this->toFloat() * rhs.toFloat()) );
}

Fixed Fixed::operator/( Fixed const & rhs )  {

	return ( Fixed( this->toFloat() / rhs.toFloat()) );
}

bool Fixed::operator>( Fixed const & rhs ) const {

	return ( (this->_number > rhs._number ? 1 : 0) );
}

bool Fixed::operator<( Fixed const & rhs ) const {

	return ( (this->_number < rhs._number ? 1 : 0) );
}

bool Fixed::operator>=( Fixed const & rhs ) const {

	return ( (this->_number >= rhs._number ? 1 : 0) );
}

bool Fixed::operator<=( Fixed const & rhs ) const {

	return ( (this->_number <= rhs._number ? 1 : 0) );
}

bool Fixed::operator==( Fixed const & rhs ) const {

	return ( (this->_number == rhs._number ? 1 : 0) );
}

bool Fixed::operator!=( Fixed const & rhs ) const {

	return ( (this->_number == rhs._number ? 0 : 1) );
}

Fixed	&Fixed::operator++( void ) {

	this->_number += 1;
	return *this;
}

Fixed	Fixed::operator++( int ) {

	Fixed	postIncrement( *this );
	
	operator++();
	return ( postIncrement ); 
}

Fixed	Fixed::operator--( int ) {

	Fixed	postDecrement( *this );
	
	operator--();
	return ( postDecrement ); 
}

Fixed	&Fixed::operator--( void ) {

	this->_number -= 1;
	return *this;
}

std::ostream	& operator<<( std::ostream & o, Fixed const & rhs ) {

	o << rhs.toFloat( );
    return o ;
}
