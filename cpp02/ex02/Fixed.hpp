#ifndef FIXED_H
# define FIXED_H

#include <iostream>

class Fixed
{
private:
	int					_number;
	static const int	_bits = 8; 
public:
	Fixed( void );  //constructeur par default
	Fixed( int const raw );
	Fixed( float const raw );
	Fixed( Fixed const &fix) ;    //constructeur par copie
	~Fixed();   //destructeur
	Fixed& operator=( Fixed const & rhs ) ;	//operateur d'affectation

	bool operator>( Fixed const & rhs ) const ;
	bool operator<( Fixed const & rhs ) const ;
	bool operator>=( Fixed const & rhs ) const ;
	bool operator<=( Fixed const & rhs ) const ;
	bool operator==( Fixed const & rhs ) const ;
	bool operator!=( Fixed const & rhs ) const ;
	
	Fixed operator+( Fixed const & rhs ) ;
	Fixed operator-( Fixed const & rhs ) ;
	Fixed operator*( Fixed const & rhs ) ;
	Fixed operator/( Fixed const & rhs ) ;

	Fixed operator++( int );
	Fixed & operator++( void );
	Fixed operator--( int );
	Fixed & operator--( void );

	static Fixed const &	min(Fixed & a, Fixed & b);
	static Fixed const &	max(Fixed & a, Fixed & b);
	static Fixed const &	min(const Fixed & a, const Fixed & b);
	static Fixed const &	max(const Fixed & a, const Fixed & b);
	
	void    setRawBits( int const raw );
	int     getRawBits( void ) const;
	float	toFloat( void ) const;
	int		toInt( void ) const;
};

std::ostream	& operator<<( std::ostream & o, Fixed const & rhs );

#endif

