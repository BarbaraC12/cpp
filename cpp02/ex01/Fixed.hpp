#ifndef FIXED_H
# define FIXED_H

#include <iostream>

class Fixed
{
private:
	int					_number;
	static const int	_bits = 8; 
public:
	Fixed( void );  //constructeur par default
	Fixed( int const raw );
	Fixed( float const raw );
	Fixed( Fixed const &fix) ;    //constructeur par copie
	~Fixed();   //destructeur
	
	Fixed & operator=( Fixed const & rhs ) ;     //operateur d'affectation

	void    setRawBits( int const raw );
	void    setRawBits( float const raw );
	int     getRawBits( void ) const;
	float	toFloat( void ) const;
	int		toInt( void ) const;
};

std::ostream	& operator<<( std::ostream & o, Fixed const & rhs );

#endif
