#include <cmath>
#include <iostream>
#include "Fixed.hpp"

Fixed::Fixed( void ) :_number( 0 ) {

	std::cout << "\033[0;32mDefault constructor called\033[0m" << std::endl;
}

Fixed::Fixed( const int iraw ) {

	std::cout << "\033[0;32mINT constructor called\033[0m" << std::endl;
    this->_number = (iraw << this->_bits);
}

Fixed::Fixed( const float fraw ) {

	std::cout << "\033[0;32mFLOAT constructor called\033[0m" << std::endl;
    this->_number = roundf(fraw * (1 << this->_bits));
}

Fixed::Fixed( Fixed const & rhs ) {

	std::cout << "\033[0;34mCopy constructor called\033[0m" << std::endl;
    *this = rhs;
}

Fixed::~Fixed() {

	std::cout << "\033[0;31mDestructor called\033[0m" << std::endl;
}

int		Fixed::getRawBits( void ) const {

	return (this->_number);
}

void	Fixed::setRawBits( int const raw ) {

	this->_number = raw;
}

int		Fixed::toInt( void ) const {

	return ( this->_number >> this->_bits );
}

float	Fixed::toFloat( void ) const {

	return ( (float)this->_number / ( 1 << this->_bits) );
}

Fixed& Fixed::operator=( Fixed const & rhs ) {

	std::cout << "\033[0;34mCOPY assignment operator called\033[0m" << std::endl;
    this->_number = rhs.getRawBits();
    return *this ;
}

std::ostream	& operator<<( std::ostream & o, Fixed const & rhs ) {

	o << rhs.toFloat( );
    return o ;
}
