#include "Fixed.hpp"
#include <iostream>

int main( void ) {

    {
        Fixed a;
        Fixed const b( 10 );
        Fixed const c( 42.42f );
        Fixed const d( b );

        a = Fixed( 1234.4321f );

        std::cout << "a is \033[1;37m" << a << "\033[0m as Float and \033[1;37m";
        std::cout << a.toInt() << "\033[0m as integer" << std::endl;
        std::cout << "b is \033[1;37m" << b << "\033[0m as Float and \033[1;37m";
        std::cout << b.toInt() << "\033[0m as integer" << std::endl;
        std::cout << "c is \033[1;37m" << c << "\033[0m as Float and \033[1;37m";
        std::cout << c.toInt() << "\033[0m as integer" << std::endl;
        std::cout << "d is \033[1;37m" << d << "\033[0m as Float and \033[1;37m";
        std::cout << d.toInt() << "\033[0m as integer" << std::endl;
    }
    return 0;
}

/*
int main( void ) {

    Fixed a;
    Fixed const b( 10 );
    Fixed const c( 42.42f );
    Fixed const d( b );

    a = Fixed( 1234.4321f );
    
    std::cout << "a is " << a << std::endl;
    std::cout << "b is " << b << std::endl;
    std::cout << "c is " << c << std::endl;
    std::cout << "d is " << d << std::endl;
    std::cout << "a is " << a.toInt() << " as integer" << std::endl;
    std::cout << "b is " << b.toInt() << " as integer" << std::endl;
    std::cout << "c is " << c.toInt() << " as integer" << std::endl;
    std::cout << "d is " << d.toInt() << " as integer" << std::endl;
    
    return 0;
}
*/