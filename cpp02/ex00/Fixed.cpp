#include <iostream>
#include "Fixed.hpp"

Fixed::Fixed( void ) {

    this->_entier = 0;
	std::cout << "\033[0;32mDefault constructor called\033[0m" << std::endl;
}

Fixed::Fixed( Fixed const &fix ) {

	std::cout << "\033[0;34mCopy constructor called\033[0m" << std::endl;
    *this = fix;
}

Fixed::~Fixed() {

	std::cout << "\033[0;31mDestructor called\033[0m" << std::endl;
}


int		Fixed::getRawBits( void ) const{

	std::cout << "getRawBits member function called" << std::endl;
	return (this->_entier);
}

void	Fixed::setRawBits( int const raw ) {

	this->_entier = raw;
}

Fixed& Fixed::operator=( Fixed const &fix ) {

	std::cout << "\033[0;34mCopy assignment operator called\033[0m" << std::endl;
    if ( this != &fix )
        this->_entier = fix.getRawBits();
    return *this ;
}