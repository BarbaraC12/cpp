#ifndef FIXED_H
# define FIXED_H

class Fixed
{
private:
	int					_entier;
	static const int	_bits = 8; 
public:
	Fixed( void );  //constructeur par default
	Fixed( Fixed const &fix) ;    //constructeur par copie
	~Fixed();   //destructeur
	Fixed &operator=( Fixed const &fix ) ;     //operateur d'affectation

	int     getRawBits( void ) const;
	void    setRawBits( int const raw );
};

#endif