#include "Span.hpp"
#include <ctime>

int main() {

	std::srand( time(NULL) );
	std::cout << "\033[1;33m#################   ARRAY OF 5\033[0m\n";
	{
		Span	sp(5);
		int		i;

		try {
			std::cout << "\033[1mTry to add 5 numbers\033[0m\n";
			std::cout << "Before   :   " << sp.getSize() << "/" << sp.getLengh() << std::endl;
			for (i = 0 ; i < 5 ; ++i)
				sp.addNumber(i);
			std::cout << "After    :   " << sp.getSize() << "/" << sp.getLengh() << std::endl;
		}
		catch (std::exception const &e) {
			std::cout << e.what() << std::endl;
		}
		std::cout << sp << std::endl;
	}
	std::cout << "\033[1;33m#################   ARRAY OF 10 000\033[0m\n";
	{
		Span	sp(10100);

		try {
			std::cout << "\033[1mTry to add 10 000 numbers\033[0m\n";
			std::cout << "Before   :   " << sp.getSize() << "/" << sp.getLengh() << std::endl;
			sp.addNumberRange( 10000 );
			std::cout << "After    :   " << sp.getSize() << "/" << sp.getLengh() << std::endl;
			std::cout << "Shortest span  : " << sp.shortestSpan() << std::endl;
			std::cout << "Longest span   : " << sp.longestSpan() << std::endl;
		}
		catch (std::exception const &e) {
			std::cout << e.what() << std::endl;
		}

	}
	std::cout << "\033[1;33m#################   EXCEPTION\033[0m\n";
	{
		Span	sp(2);
		int		i;

		try {
			std::cout << "\033[1mTry to add 5 numbers\033[0m\n";
			std::cout << "Before   :   " << sp.getSize() << "/" << sp.getLengh() << std::endl;
			for (i = 0 ; i < 5 ; ++i)
				sp.addNumber(i);
		}
		catch (std::exception const &e) {
			std::cout << e.what() << std::endl;
		}
		std::cout << "After    :   " << sp.getSize() << "/" << sp.getLengh() << std::endl;
		std::cout << sp << std::endl;
	}
	std::cout << "\033[1;33m########### SHORTEST - LONGEST\033[0m\n";
	{
		Span sp ( 5 );

		sp.addNumberRange(5);
		std::cout << "Shortest span  : " << sp.shortestSpan() << std::endl;
		std::cout << "Longest span   : " << sp.longestSpan() << std::endl;
		std::cout << sp << std::endl;

	}
	return 0;
}