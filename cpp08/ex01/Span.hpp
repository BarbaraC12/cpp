#ifndef SPAN_HPP
# define SPAN_HPP

# include <iostream>
# include <exception>
# include <algorithm>
# include <iterator>
# include <vector>
# include <list>
# include <string>

class Span {

	public:

		Span( unsigned int N = 0U);
		Span( Span const & src );
		~Span();

		Span &					operator=( Span const & rhs );
		unsigned int			shortestSpan( void ) const ;
		unsigned int			longestSpan( void ) const ;
		void					addNumber( int num );
		void					addNumberRange( unsigned int spanSize );

		unsigned int const		&getLengh( void ) const ;
		unsigned int const		&getSize( void ) const ;
		std::vector<int> const	&getTab( void ) const ;

	private:

		unsigned int			_N;
		unsigned int			_size;
		std::vector<int>		_array;

		class	NoPlaceException: public std::exception {
			public:
				virtual const char* what() const throw() {
					return ("\033[1;35m!! E \033[0mOut of place");
				}
		};
		class	NoRangeException: public std::exception {
			public:
				virtual const char* what() const throw() {
					return ("\033[1;35m!! E \033[0mList to short");
				}
		};

};

std::ostream &					operator<<( std::ostream & o, Span const & i );

#endif /* ************************************************************ SPAN_H */