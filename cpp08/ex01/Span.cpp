#include "Span.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

Span::Span( unsigned int N ) :_N(N), _size(0), _array() { }

Span::Span( const Span & src ) :_N(src.getLengh()), _size(src.getSize()), _array(src.getTab()) { }

/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

Span::~Span( void ) { }

/*
** --------------------------------- OVERLOAD ---------------------------------
*/

Span &					Span::operator=( Span const & rhs ) {

	if ( this != &rhs ) {
		this->_N = rhs.getLengh();
		this->_size = rhs.getSize();
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, Span const & i ) {

	o << "\033[1;34m----- SPAN CONTENT -----\033[0m\n";
	o << "Size  MAX -> " << i.getLengh() << std::endl;
	o << "Size      -> " << i.getSize() << std::endl;
	o << "Content   -> { ";
	std::vector<int>::const_iterator	ptr;
	ptr = i.getTab().begin();
	for (; ptr != i.getTab().end(); ptr++ ) {
		o << *ptr;
		if ( ptr + 1 != i.getTab().end() )
			o << ", ";
	}
	o <<  " }.\n";
	return o;
}


/*
** --------------------------------- METHODS ----------------------------------
*/

void					Span::addNumber( int nb ) {

	if ( _size == _N )
		throw NoPlaceException( );
	_array.push_back( nb );
	_size++;
}

void					Span::addNumberRange( unsigned int rangeSize ) {

	for ( unsigned int i(0); i < rangeSize; i++ ) {
		this->addNumber( std::rand() % (rangeSize * 2));
	}
}


unsigned int			Span::shortestSpan( void ) const  {

	if ( this->_size < 2 )
		throw NoRangeException( );
	
	std::vector<int>			tmp( _array );
	unsigned int				shortest( tmp.end() - tmp.begin() );
	std::sort( tmp.begin(), tmp.end() );
	std::vector<int>::iterator	ptr( tmp.begin() );
	unsigned int			a( *ptr );
	while ( ++ptr != tmp.end() ) {
		unsigned int			b( *ptr );
		shortest = ( (b - a) < shortest ) ? b - a : shortest ;
		a = b;
	}
	return shortest;
}

unsigned int			Span::longestSpan( void ) const  {

	if ( this->_size < 2 )
		throw NoRangeException( );

	return ( *max_element(_array.begin(), _array.end())
			- *min_element(_array.begin(), _array.end()) );
}


/*
** --------------------------------- ACCESSOR ---------------------------------
*/

unsigned int const			&Span::getLengh( void ) const  {

	return this->_N;
}

unsigned int const			&Span::getSize( void ) const  {

	return this->_size;
}

std::vector<int> const	&Span::getTab( void ) const  {

	return this->_array;
}

/* ************************************************************************** */