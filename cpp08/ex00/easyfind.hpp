#ifndef EASYFIND_HPP
# define EASYFIND_HPP

# include <exception>
# include <algorithm>
# include <iterator>
# include <iostream>
# include <string>

class notFoundException : public std::exception {
	public:
		virtual const char* what() const throw() {
			return ("\033[1;35m!! E \033[0mElement not found");
		}
};

template< typename T >
typename T::iterator	easyfind( T & list, int const toFind ) {

	typename T::iterator	ret(std::find(list.begin(), list.end(), toFind));
	
	if ( ret == list.end() )
		throw notFoundException();
	return ret;

}

template< typename T >
typename T::const_iterator	easyfind( T const & list, int const toFind ) {

	typename T::const_iterator	ret(std::find(list.begin(), list.end(), toFind));
	
	if ( ret == list.end() )
		throw notFoundException();
	return ret;

}

#endif