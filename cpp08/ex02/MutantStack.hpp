#ifndef MUTANTSTACK_HPP
# define MUTANTSTACK_HPP

# include <stack>
# include <list>
# include <iostream>

template <typename T>
class MutantStack: public std::stack<T> {

	public:
		MutantStack( void ) { };
		MutantStack( const MutantStack & src ): std::stack<T>(src) { };
		virtual ~MutantStack( void ) { };

		MutantStack &		operator=( const MutantStack & rhs ) {
			if (this != &rhs) {
				static_cast< std::stack<T> >(*this) = static_cast< std::stack<T> >(rhs);
			}
			return *this;
		}

		typedef typename std::stack<T>::container_type		Mutant;

		typedef typename Mutant::iterator					iterator;
		typedef typename Mutant::const_iterator				const_iterator;
		typedef typename Mutant::reverse_iterator			reverse_iterator;
		typedef typename Mutant::const_reverse_iterator		const_reverse_iterator;

		iterator					begin( void ) { return (this->c.begin()); }

		iterator					end( void ) { return (this->c.end()); }

		reverse_iterator			rbegin( void ) { return (this->c.rbegin()); }
		
		reverse_iterator			rend( void ) { return (this->c.rend()); }

		const_iterator				begin( void ) const { return (this->c.begin()); }

		const_iterator				end( void ) const { return (this->c.end()); }

		const_reverse_iterator		rbegin( void ) const { return (this->c.rbegin()); }

		const_reverse_iterator		rend( void ) const { return (this->c.rend()); }

};

template <typename T>
std::ostream	&operator<<(std::ostream &o, MutantStack<T> const &rhs)
{
	MutantStack<int>::const_iterator	iter;

	o << "MutantStack:" << std::endl;
	for (iter = rhs.begin(); iter != rhs.end(); ++iter)
		o << "\t" << *iter << ": " << &*iter << std::endl;
	return o;
}

template <typename T>
std::ostream	&operator<<(std::ostream &o, typename MutantStack<T>::iterator const &rhs)
{
	o << "MutantStack::iterator:" << std::endl
	<< "\t" "*rhs: " << *rhs << std::endl
	<< "\t" "&*rhs: " << &*rhs << std::endl;
	return o;
}

template <typename T>
std::ostream	&operator<<(std::ostream &o, typename MutantStack<T>::const_iterator const &rhs)
{
	o << "MutantStack::const_iterator:" << std::endl
	<< "\t" "*rhs: " << *rhs << std::endl
	<< "\t" "&*rhs: " << &*rhs << std::endl;
	return o;
}

#endif
