#include "MutantStack.hpp"

int		main()
{
	{
		MutantStack<int> mstack;

		std::cout << "--- TEST PUSH POP ---"<< std::endl;
		mstack.push(5);
		mstack.push(17);
		std::cout << "Size       :" << mstack.size() << std::endl;
		std::cout << "Last       :" << mstack.top() << std::endl;
		std::cout << "--- Pop 1 element ---" << std::endl;
		mstack.pop();
		std::cout << "Size       :" << mstack.size() << std::endl;
		std::cout << "--- Push 4 elements ---" << std::endl;
		for ( int i(0); i < 4 ; ++i )
			mstack.push(i * 3 + 1);
		std::cout << "Size       :" << mstack.size() << std::endl;
		std::cout << "Last       :" << mstack.top() << std::endl;
		std::cout << std::endl;

		MutantStack<int>::iterator it = mstack.begin();
		MutantStack<int>::iterator ite = mstack.end();
		std::cout << "--- TEST ITERATOR ---"<< std::endl;
		std::cout << std::endl;
		std::cout << "Iterator   :" << *it << std::endl;
		++it;
		std::cout << "Iterator++ :" << *it << std::endl;
		--it;
		std::cout << "Iterator-- :" << *it << std::endl;
		std::cout << std::endl;
		while (it != ite) {
			std::cout << "Iterator   :" << *it << std::endl;
			++it;
		}
	}
	{
		std::cout << std::endl;
		MutantStack<int>			mut;
		std::list<int>				lst;
		MutantStack<int>::iterator	iter0;
		std::list<int>::iterator	iter1;
		int							i;

		std::cout << "--- Push 10 elements ---" << std::endl;
		for (i = 0 ; i < 10 ; ++i) {
			mut.push(i * 3 + 1);
			lst.push_back(i * 3 + 1);
		}
		std::cout 	<< "My mutantStack   : {";
		iter0 = mut.begin();
		while (iter0 != mut.end()) {
			std::cout << *iter0;
			if (++iter0 != mut.end())
				std::cout << ", ";
		}
		std::cout << "}\nList form stack  : {";
		iter1 = lst.begin();
		while (iter1 != lst.end()) {
			std::cout << *iter1;
			if (++iter1 != lst.end())
				std::cout << ", ";
		}
		std::cout << "}" << std::endl;
		std::cout << std::endl;
	}
}
