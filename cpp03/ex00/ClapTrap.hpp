#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

# include <iostream>

class ClapTrap
{
private:
	std::string		_name;
	unsigned int	_hitPoint;
	unsigned int	_energy;
	unsigned int	_damage;

public:
	ClapTrap( void );
	ClapTrap( std::string name );
	ClapTrap( ClapTrap const & src );
	~ClapTrap();

	void			attack( const std::string& target );
	void			takeDamage( unsigned int amount );
	void			beRepaired( unsigned int amount );

	void 			setName( std::string name );
	void 			setDamage( unsigned int raw );
	void 			setLive( unsigned int raw );
	void 			setEnergy( unsigned int raw );

	void 			setGetDamage( unsigned int const raw );
	void 			setNewLive( int const raw );
	void			setRestore( unsigned int const raw );
	void 			setNewEnergy( void );
	
	std::string		getName( void ) const;
	unsigned int	getLive( void ) const;
	unsigned int	getEnergy( void ) const;
	unsigned int	getDamage( void ) const;

	ClapTrap &		operator=( ClapTrap const & rhs );

};

#endif

std::ostream &			operator<<( std::ostream & o, ClapTrap const & i );