#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>
#include "ClapTrap.hpp"

static 
std::string nameGenerator( void ) {

	srand(time(NULL));
	static const std::string name[22] = {
		"Twirlbud", "Ginnattah", "Bedoo", "Ukhmuk", "Xeingem",
		"Lovedancer", "Ice Freak", "Slugslinger", "Bling-P", "Maria",
		"Nekoda","JacynthVampyre", "Christophoros", "Notus", "Gaios",
		"Eutychos", "Hyginos", "Zelpha", "Hugo", "Bernice", "Bcano"};

	return (name[std::rand() % 21]);
}


int	main( void ) {

    std::cout << "\033[1;33m\t##########  CLAPTRAP  ########## \033[0m" << std::endl;

	ClapTrap	comrade0;
	ClapTrap	comrade1( nameGenerator( ) );
	sleep( 1);
	ClapTrap	comrade2( nameGenerator( ) );

	comrade0.attack( comrade2.getName() );
	comrade2.takeDamage(0);
	std::cout << "\033[0;35m>>\033[0m " << comrade0.getName() << " least " << comrade0.getEnergy() << " energy" << std::endl;
	std::cout << std::endl;
	comrade1.attack( comrade2.getName() );
	comrade2.takeDamage(0);
	comrade2.beRepaired(1);
	std::cout << "\033[0;35m>>\033[0m " << comrade2.getName() << " least " << comrade2.getEnergy() << " energy" << std::endl;
	std::cout << std::endl;
	std::cout << "\033[0;36m\t== BEGIN ASSIGNATION ==\033[0m" << std::endl; 
	comrade0 = comrade2;
   	std::cout << "\033[0;36m\t== END ASSIGNATION ==\033[0m" << std::endl; 
	std::cout << std::endl;

	std::cout << "\033[0;35m>>\033[0m " << comrade0.getName() << " least " << comrade0.getLive() << " lives" << std::endl;
	comrade1.attack( comrade0.getName() );
	comrade0.takeDamage(12);
	std::cout << "\033[0;35m>>\033[0m " << comrade0.getName() << " least " << comrade0.getLive() << " lives" << std::endl;

	return (0);
}
