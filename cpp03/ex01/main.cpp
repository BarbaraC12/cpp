#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>
#include "ClapTrap.hpp"
#include "ScavTrap.hpp"

static 
std::string nameGenerator( void ) {

	srand(time(NULL));
	static const std::string name[22] = {
		"Twirlbud", "Ginnattah", "Bedoo", "Marvin", "Xeingem",
		"Lovedancer", "Ice Freak", "Slugslinger", "Bling-P", "Maria",
		"Nekoda","JacynthVampyre", "Christophoros", "Notus", "Gaios",
		"Eutychos", "Hyginos", "Zelpha", "Hugo", "Bernice", "Bcano"};

	return (name[std::rand() % 22]);
}


int	main( void ) {

    std::cout << "\033[1;33m\t##########  CLAPTRAP + SCAVTRAP  ########## \033[0m" << std::endl;

	ClapTrap	comrade1( nameGenerator( ) );
	ClapTrap	comrade2;
	sleep( 1 );
	ScavTrap	comrade0;
	ScavTrap	comrade42( nameGenerator( ) );

	comrade0.attack( comrade2.getName() );
	comrade2.takeDamage( 20 );
	comrade1.attack( comrade2.getName() );
	comrade2.takeDamage( 20 );
	sleep( 1 );
	{

		std::cout << "\033[0;36m\t== BEGIN CLAP ASSIGNATION ==\033[0m" << std::endl; 
		comrade1 = ClapTrap( nameGenerator( ) );
		std::cout << "\033[0;36m\t== END CLAP ASSIGNATION ==\033[0m" << std::endl; 
	}
	comrade2.beRepaired( 10 );
	comrade1.attack( comrade0.getName() );
	comrade0.takeDamage( 20 );
	sleep( 1 );
	std::cout << "\033[0;36m\t== BEGIN SCAV ASSIGNATION ==\033[0m" << std::endl; 
	comrade0 = ScavTrap( nameGenerator( ) );
   	std::cout << "\033[0;36m\t== END SCAV ASSIGNATION ==\033[0m" << std::endl; 
	comrade42.guardGate( );
	comrade0.guardGate( );
	return (0);
}
