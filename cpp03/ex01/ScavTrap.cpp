#include "ScavTrap.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

ScavTrap::ScavTrap( void ) : ClapTrap( "Unknow" ) {
	
	std::cout << "\033[0;32mNew Robot summoned: \033[0m" << this->_name << std::endl;

}

ScavTrap::ScavTrap( std::string name ) : ClapTrap( name ) {

	std::cout << "\033[0;32mNew Robot summoned: \033[0m" << this->_name << std::endl;

}

ScavTrap::ScavTrap( const ScavTrap & src ) {

	*this = src;
	// std::cout << "New Robot by copy summoned: " << this->_name << std::endl;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

ScavTrap::~ScavTrap() {

	std::cout << "\033[0;31mRobot " << this->_name << " has been destroyed\033[0m" << std::endl;
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

ScavTrap &				ScavTrap::operator=( ScavTrap const & rhs )
{
	if ( this != &rhs )
	{
		std::cout << "Robot " << getName() << " has been reassigned:" << std::endl;
		this->_name = rhs.getName();
		std::cout << "\033[0;36m";
		std::cout << "==== Name = " << this->_name << std::endl;
		this->_hitPoint = rhs.getLive();
		std::cout << "==== Live = " << this->_hitPoint << std::endl;
		this->_energy = rhs.getEnergy();
		std::cout << "==== Energy = " << this->_energy << std::endl;
		this->_damage = rhs.getDamage();
		std::cout << "==== Damage = " << this->_damage << std::endl;
		std::cout << "\033[0m";
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, ScavTrap const & i ) {

	o << "Name = " << i.getName();
	o << "live = " << i.getLive();
	o << "Energy = " << i.getEnergy();
	o << "Damage = " << i.getDamage();
	
	return o;
}




/*
** --------------------------------- METHODS ----------------------------------
*/

void			ScavTrap::guardGate( void ) {

	std::cout << "Robot " << this->_name << " is enter in guard Mode " << std::endl;
}

void			ScavTrap::attack( const std::string& target ) {

	if ( getEnergy( ) > 0 )
	{
		setEnergy( );
		std::cout << this->_name << " attack " << target << " and causing  "
			<< this->_damage << " point of damage!" << std::endl;
	}
	else
		std::cout << "Oh not! I am out of energy!";

}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

/* ************************************************************************** */