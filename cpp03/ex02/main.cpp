#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>
#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"

static 
std::string nameGenerator( void ) {

	srand(time(NULL));
	static const std::string name[22] = {
		"Twirlbud", "Ginnattah", "Bedoo", "Marvin", "Xeingem",
		"Lovedancer", "Ice Freak", "Slugslinger", "Bling-P", "Maria",
		"Nekoda","JacynthVampyre", "Christophoros", "Notus", "Gaios",
		"Eutychos", "Hyginos", "Zelpha", "Hugo", "Bernice", "Bcano"};

	return (name[std::rand() % 21]);
}


int	main( void ) {

    std::cout << "\033[1;33m\t##########  CLAPTRAP + SCAVTRAP + FRAGTRAP ########## \033[0m" << std::endl;

	ScavTrap	comrade42( nameGenerator( ) );
	FragTrap	comrade0;
	sleep( 1 );
	ClapTrap	comrade1( nameGenerator( ) );
	sleep( 1 );
	ClapTrap	comrade2( nameGenerator( ) );

	comrade0.attack( comrade2.getName() );
	comrade2.takeDamage( 20 );
	comrade1.attack( comrade2.getName() );
	comrade2.takeDamage( 20 );
	comrade2.beRepaired( 10 );
	comrade1.attack( comrade0.getName() );
	comrade0.takeDamage( 20 );
	sleep( 1 );
	std::cout << "\033[0;36m\t== BEGIN FRAG ASSIGNATION ==\033[0m" << std::endl; 
	comrade0 = FragTrap( nameGenerator( ) );
   	std::cout << "\033[0;36m\t== END FRAG ASSIGNATION ==\033[0m" << std::endl; 
	comrade42.guardGate( );
	comrade0.highFivesGuys( );
	return (0);
}
