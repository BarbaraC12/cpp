#include "ClapTrap.hpp"
#include <iostream>

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

ClapTrap::ClapTrap( void ) 
    :_name("Unknown"), _hitPoint(100), _energy(50), _damage(20) {

	std::cout << "\033[0;32mNew player summoned: \033[0m" << this->_name << std::endl;
}

ClapTrap::ClapTrap( std::string name ) 
    :_name(name), _hitPoint(100), _energy(50), _damage(20) {

	std::cout << "\033[0;32mNew player summoned: \033[0m" << this->_name << std::endl;
}

ClapTrap::ClapTrap( const ClapTrap & src ) {

	*this = src;
	std::cout << "New player by copy summoned: " << this->_name << std::endl;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

ClapTrap::~ClapTrap() {

	std::cout << "\033[0;31mPlayer " << this->_name << " has been destroyed\033[0m" << std::endl;
}

/*
** --------------------------------- OVERLOAD ---------------------------------
*/

ClapTrap &				ClapTrap::operator=( ClapTrap const & rhs )
{
	if ( this != &rhs )
	{
		std::cout << "ClapTrap " << getName() << " has been reassigned:" << std::endl;
		this->_name = rhs.getName();
		std::cout << "\033[0;36m";
		std::cout << "==== Name = " << this->_name << std::endl;
		this->_hitPoint = rhs.getLive();
		std::cout << "==== Live = " << this->_hitPoint << std::endl;
		this->_energy = rhs.getEnergy();
		std::cout << "==== Energy = " << this->_energy << std::endl;
		this->_damage = rhs.getDamage();
		std::cout << "==== Damage = " << this->_damage << std::endl;
		std::cout << "\033[0m";
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, ClapTrap const & i )
{
	o << "Name = " << i.getName();
	o << "live = " << i.getLive();
	o << "Energy = " << i.getEnergy();
	o << "Damage = " << i.getDamage();
	
	return o;
}


/*
** --------------------------------- METHODS ----------------------------------
*/

void			ClapTrap::attack( const std::string& target ) {

	if ( getEnergy( ) > 0 )
	{
		setEnergy( );
		std::cout << this->_name << " attack " << target
			<< ", causing  " << this->_damage << " point of damage!" << std::endl;
	}
	else
		std::cout << this->_name << " is out of energy!";

}

void			ClapTrap::takeDamage( unsigned int amount ) {

	if (getLive( ) - amount >= 0 )
	{
		std::cout << this->_name << " get " << amount << " point of damage! ";
		this->setDamage( amount );
		std::cout << "Least " << getLive( ) << " lives." << std::endl;
	}
	else 
		std::cout << this->_name << " is out of live!" << std::endl;

}

void			ClapTrap::beRepaired( unsigned int amount ) {

	if ( getEnergy( ) > 0 )
	{
		setEnergy( );
		this->setRestore( amount );
		std::cout << this->_name << " take a nap  " << amount 
			<< " point of repaired! Now least " << getLive( ) << " lives." << std::endl;
	}
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/

void			ClapTrap::setLive( int const raw ) {

	this->_hitPoint += raw;
}

void			ClapTrap::setRestore( unsigned int const raw ) {

	this->_hitPoint += raw;
}

void			ClapTrap::setEnergy( void ) {

	this->_energy -= 1;
}

void			ClapTrap::setDamage( unsigned int const raw ) {

	this->_hitPoint -= raw;
}

std::string		ClapTrap::getName( void ) const {

	return (this->_name);
}

unsigned int	ClapTrap::getLive( void ) const {

	return (this->_hitPoint);
}

unsigned int	ClapTrap::getEnergy( void ) const {

	return (this->_energy);
}

unsigned int	ClapTrap::getDamage( void ) const {

	return (this->_damage);
}
/* ************************************************************************** */