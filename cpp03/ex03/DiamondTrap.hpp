#ifndef DIAMONDTRAP_HPP
# define DIAMONDTRAP_HPP

# include "FragTrap.hpp"
# include "ScavTrap.hpp"
# include "ClapTrap.hpp"
# include <iostream>
# include <string>

class DiamondTrap : public FragTrap , public ScavTrap {

	public:

		DiamondTrap( void );
		DiamondTrap( std::string name );
		DiamondTrap( DiamondTrap const & src );
		virtual ~DiamondTrap();

		DiamondTrap &		operator=( DiamondTrap const & rhs );

		void				whoAmI( );

	protected:
		std::string			_name;

};

std::ostream &			operator<<( std::ostream & o, DiamondTrap const & i );

#endif /* ***************************************************** DIAMONDTRAP_H */