#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>
#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"
#include "DiamondTrap.hpp"

static 
std::string nameGenerator( void ) {

	srand(time(NULL));
	static const std::string name[22] = {
		"Twirlbud", "Ginnattah", "Bedoo", "Marvin", "Xeingem",
		"Lovedancer", "Ice Freak", "Slugslinger", "Jodufour", "Maria",
		"Nekoda", "Fcatinau", "Christophoros", "Notus", "Gaios",
		"Eutychos", "Hyginos", "Zelpha", "Hugo", "Bernice", "Bcano"};

	return (name[std::rand() % 21]);
}


int	main( void ) {

    std::cout << "\033[1;33m\t##########  DIAMONDTRAP ########## \033[0m" << std::endl;

	DiamondTrap		comrade68;
	DiamondTrap		comrade69( nameGenerator( ) );

	comrade69.attack(comrade68.getName());
	comrade68.whoAmI();
	comrade69.whoAmI();

	return (0);
}
