#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

# include "ClapTrap.hpp"
# include <iostream>
# include <string>

class FragTrap : virtual public ClapTrap {

	public:

		FragTrap();
		FragTrap( std::string name );
		FragTrap( FragTrap const & src );
		virtual ~FragTrap();

		void			highFivesGuys( void );

		FragTrap &		operator=( FragTrap const & rhs );

	protected:
		int				_hitPoint;
		int				_damage;

};

std::ostream &			operator<<( std::ostream & o, FragTrap const & i );

#endif /* ******************************************************** FRAGTRAP_H */