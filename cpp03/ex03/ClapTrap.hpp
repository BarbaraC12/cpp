#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

# include <iostream>

class ClapTrap
{
protected:
	std::string		_name;
	unsigned int	_hitPoint;
	unsigned int	_energy;
	unsigned int	_damage;

public:
	ClapTrap( void );
	ClapTrap( std::string name );
	ClapTrap( ClapTrap const & src );
	virtual ~ClapTrap();

	void			attack( const std::string& target );
	void			takeDamage( unsigned int amount );
	void			beRepaired( unsigned int amount );
	std::string		getName( void ) const;
	void 			setLive( int const raw );
	unsigned int	getLive( void ) const;
	void 			setEnergy( void );
	unsigned int	getEnergy( void ) const;
	void			setRestore( unsigned int const raw );
	void 			setDamage( unsigned int const raw );
	unsigned int	getDamage( void ) const;

	ClapTrap &		operator=( ClapTrap const & rhs );

};

#endif

std::ostream &			operator<<( std::ostream & o, ClapTrap const & i );