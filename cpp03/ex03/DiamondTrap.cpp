#include "DiamondTrap.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

DiamondTrap::DiamondTrap( void ) 
    :ClapTrap( "Unknow_clap_name" ),FragTrap( "Unknow_clap_name" ), ScavTrap( "Unknow_clap_name" ), _name( "Unknown" ) {
	
	this->_hitPoint = FragTrap::getLive();
	this->_damage = FragTrap::getDamage();
	this->_energy = ScavTrap::getEnergy();
	std::cout << "\033[0;32mNew Monster summoned: \033[0m" << this->_name << std::endl;
}

DiamondTrap::DiamondTrap( std::string name ) 
    :ClapTrap( name + "_clap_name" ), FragTrap( name + "_clap_name" ), ScavTrap( name + "_clap_name" ), _name( name ) {

	this->_hitPoint = FragTrap::getLive();
	this->_damage = FragTrap::getDamage();
	this->_energy = ScavTrap::getEnergy();
	std::cout << "\033[0;32mNew Monster summoned: \033[0m" << this->_name << std::endl;
}

DiamondTrap::DiamondTrap( const DiamondTrap & src ) {

	*this = src;
	std::cout << "New Monster by copy summoned: " << this->_name << std::endl;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

DiamondTrap::~DiamondTrap() {

	std::cout << "\033[0;31mMonster " << this->_name << " has been destroyed\033[0m" << std::endl;
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

DiamondTrap &				DiamondTrap::operator=( DiamondTrap const & rhs )
{
	if ( this != &rhs ) 	{
	
		std::cout << "Monster " << getName() << " has been reassigned:" << std::endl;
		this->_name = rhs.getName();
		std::cout << "\033[0;36m";
		std::cout << "==== Name = " << this->_name << std::endl;
		this->_hitPoint = rhs.getLive();
		std::cout << "==== Live = " << this->_hitPoint << std::endl;
		this->_energy = rhs.getEnergy();
		std::cout << "==== Energy = " << this->_energy << std::endl;
		this->_damage = rhs.getDamage();
		std::cout << "==== Damage = " << this->_damage << std::endl;
		std::cout << "\033[0m";
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, DiamondTrap const & i ) {

	o << "Name = " << i.getName();
	o << "live = " << i.getLive();
	o << "Energy = " << i.getEnergy();
	o << "Damage = " << i.getDamage();
	
	return o;
}


/*
** --------------------------------- METHODS ----------------------------------
*/

void				DiamondTrap::whoAmI( ) {

	std::cout << "Name: " << ClapTrap::_name << " Monster Name: " << this->_name << std::endl;
}


/*
** --------------------------------- ACCESSOR ---------------------------------
*/


/* ************************************************************************** */