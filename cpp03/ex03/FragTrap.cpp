#include "FragTrap.hpp"

/*
** ------------------------------- CONSTRUCTOR --------------------------------
*/

FragTrap::FragTrap() {
	
	this->_name = "Unknow";
	this->_hitPoint = 100;
	this->_energy = 100;
	this->_damage = 30;
	std::cout << "\033[0;32mNew Frag player summoned: \033[0m" << this->_name << std::endl;
}

FragTrap::FragTrap( std::string name ) {
	
	this->_name = name;
	this->_hitPoint = 100;
	this->_energy = 100;
	this->_damage = 30;
	std::cout << "\033[0;32mNew Frag player summoned: \033[0m" << this->_name << std::endl;
}

FragTrap::FragTrap( const FragTrap & src ) {

	*this = src;
	std::cout << "New Frag player by copy summoned: " << this->_name << std::endl;
}


/*
** -------------------------------- DESTRUCTOR --------------------------------
*/

FragTrap::~FragTrap(){

	std::cout << "\033[0;31mFrag player " << this->_name << " has been destroyed\033[0m" << std::endl;
}


/*
** --------------------------------- OVERLOAD ---------------------------------
*/

FragTrap &				FragTrap::operator=( FragTrap const & rhs ) {

	if ( this != &rhs )
	{
		std::cout << "Robot " << getName() << " has been reassigned:" << std::endl;
		this->_name = rhs.getName();
		std::cout << "\033[0;36m";
		std::cout << "==== Name = " << this->_name << std::endl;
		this->_hitPoint = rhs.getLive();
		std::cout << "==== Live = " << this->_hitPoint << std::endl;
		this->_energy = rhs.getEnergy();
		std::cout << "==== Energy = " << this->_energy << std::endl;
		this->_damage = rhs.getDamage();
		std::cout << "==== Damage = " << this->_damage << std::endl;
		std::cout << "\033[0m";
	}
	return *this;
}

std::ostream &			operator<<( std::ostream & o, FragTrap const & i ) {

	o << "Name = " << i.getName();
	o << "live = " << i.getLive();
	o << "Energy = " << i.getEnergy();
	o << "Damage = " << i.getDamage();
	
	return o;
}


/*
** --------------------------------- METHODS ----------------------------------
*/

void			FragTrap::highFivesGuys( void ) {

	std::cout << this->_name << " wanna have high fives with you guy !" << std::endl;
}

/*
** --------------------------------- ACCESSOR ---------------------------------
*/


/* ************************************************************************** */