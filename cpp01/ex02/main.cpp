#include <iostream>

int main (void) {

    std::string     str = "HI THIS IS BRAIN";
    std::string*    strPTR = &str;
    std::string&    strREF = str;

    std::cout << "\033[1;33m*********   PART 1: Memory Address  *********\033[0m" << std::endl;
    std::cout << "str      =   " << &str << std::endl;
    std::cout << "strPTR   =   " << &strPTR << std::endl;
    std::cout << "strREF   =   " << &strREF << std::endl;
    std::cout << std::endl;
    std::cout << "\033[1;33m*********   PART 2: Memory Value    *********\033[0m" << std::endl;
    std::cout << "str      =   " << str << std::endl;
    std::cout << "strPTR   =   " << strPTR << std::endl;
    std::cout << "strREF   =   " << strREF << std::endl;
    std::cout << std::endl;
    std::cout << "\033[1;33m*********   PART 3: Modify Value    *********\033[0m" << std::endl;
    *strPTR = "I LOSE MIND";
    std::cout << "str      =   " << str << " | address --> " << &str << std::endl;
    std::cout << "strPTR   =   " << strPTR << " | address --> " << &strPTR << std::endl;
    std::cout << "strREF   =   " << strREF << " | address --> " << &strREF << std::endl;
    
    return 0;
}
