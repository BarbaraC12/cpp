#include "Zombie.hpp"

Zombie::Zombie( void ) {

    std::cout << "\033[0;32mA new Zombie was back to life among the zombies Horde.\033[0m"<< std::endl;
    return ;
}

Zombie::~Zombie() {

    std::cout << "\033[0;31m" << this->_name << " turn back to dust.\033[0m"<< std::endl;
    return ;
}

void    Zombie::nominate( std::string name ) {

    this->_name = name;
}

void    Zombie::announce( void ) const {

    std::cout << this->_name << ": BraiiiiiiinnnzzzZ ... look so delicious" << std::endl;
}
