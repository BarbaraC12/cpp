#include <cstdlib>
#include <iostream>
#include <ctime>
#include "Zombie.hpp"

#define HORDE_SIZE  8

int main( void ) {

    Zombie* horde;
    int     hordeSize = HORDE_SIZE;

    std::cout << "\033[1;34mHorde of " << HORDE_SIZE<< " zombies invoked:\033[0m" << std::endl;
    horde = zombieHorde( hordeSize, "Lambda" );
    for ( int i = 0; i < hordeSize; i++)
    {
        std::cout << "\033[0;33m" << i + 1 << "\033[0m: ";
        horde[i].announce();
    }
    delete [] horde;

    return 0;
}
