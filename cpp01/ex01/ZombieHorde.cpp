#include <iostream>
#include "Zombie.hpp"

Zombie*	zombieHorde( int N, std::string name ) {
    
    Zombie* person = new Zombie[N];

    for ( int i = 0; i < N; i++)
        person[i].nominate( name );

    return person;
}
