#include "Karen.hpp"

Karen::Karen( void ) {

	std::cout << "=> Oh no! Karen is coming, shut up!" << std::endl << std::endl;
}

Karen::~Karen() {

   	std::cout << std::endl << "=> Phew .. Karen is finally gone!" << std::endl;
}

void	Karen::_debug( void ) {

    std::cout << "I love having extra bacon for my 7XL-double-cheese-triple-pickle-specialketchup burger. I really do !" << std::endl << std::endl;
}

void	Karen::_info( void ) {

    std::cout << "I cannot believe adding extra bacon costs more money. You didn't put enough bacon in my burger ! If you did, I wouldn't be asking for more !" << std::endl << std::endl;
}

void	Karen::_warning( void ) {

    std::cout << "I think I deserve to have some extra bacon for free. I've been coming for years whereas you started working here since last month." << std::endl << std::endl;
}

void	Karen::_error( void ) {

    std::cout << "This is unacceptable ! I want to speak to the manager now!" << std::endl;
}

void	Karen::_default( void ) {

    std::cout << "[ Probably complaining about insignificant problems ]" << std::endl;
}

void Karen::executeNow(std::string a, std::string b, void (Karen::*func)())
{
    if(a == b)
        (this->*func)();
}

void	Karen::complain( std::string level ) {

    executeNow( level, "DEBUG", &Karen::_debug );
    executeNow( level, "INFO", &Karen::_info );
    executeNow( level, "WARNING", &Karen::_warning );
    executeNow( level, "ERROR", &Karen::_error );
    executeNow( level, "DEFAULT", &Karen::_default );

}