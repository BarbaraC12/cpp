#include "Karen.hpp"

typedef struct s_switchOff	t_switch;

struct s_switchOff
{
	int				value;
	std::string		level;
};

static t_switch tolerateLevel[5] = {
	{0, "DEBUG"},
	{1, "INFO"},
	{2, "WARNING"},
	{3, "ERROR"},
	{4, ""}
};

int hashit(std::string const& inString) {

	int		i( 0 );

	while ((tolerateLevel[i].level).compare("") ) {
    	if (inString == tolerateLevel[i].level)
			break ;
		i++;
	}
	return tolerateLevel[i].value;
}

int main( int argc, char **argv ) {

	Karen   karen;
	if ( argc != 2 ) {

		std::cout << "Karen left without making wave." << std::endl;
		return 0;
	}
	int level = hashit( argv[1] );
	switch( level ) {

		case 0:
			std::cout << "\033[1;31m[ DEBUG ]\033[0m" << std::endl;
			karen.complain( "DEBUG" );
		case 1:
			std::cout <<"\033[1;31m[ INFO ]\033[0m" << std::endl;
			karen.complain( "INFO" );
		case 2:
			std::cout << "\033[1;31m[ WARNING ]\033[0m" << std::endl;
			karen.complain( "WARNING" );
		case 3:
			std::cout << "\033[1;31m[ ERROR ]\033[0m" << std::endl;
			karen.complain( "ERROR" );
			break ;
		default :
			karen.complain( "DEFAULT" );
	}

	return 0;
}