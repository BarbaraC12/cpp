#ifndef KAREN_HPP
# define KAREN_HPP

# include <iostream>

class Karen {
private:
	void	_debug( void );
	void	_info( void );
	void	_warning( void );
	void	_error( void );
	void	_default( void );

public:
	Karen( void );
	~Karen();

	void	complain( std::string level );
	void	executeNow( std::string a, std::string b, void (Karen::*func)());
};

#endif