#include "Zombie.hpp"
#include <iostream>
#include <ctime>

void	randomChump( std::string name ) {

	Zombie	lambda = Zombie( name );
	
	lambda.announce();
	return ;
}

void	randomChump( void ) {

	Zombie	lambda = Zombie( );
	
	lambda.announce();
	return ;
}
