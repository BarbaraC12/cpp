#include <iostream>
#include "Zombie.hpp"

Zombie*	newZombie( std::string name ) {
    
    Zombie* person = new Zombie( name );
    person->nominate( name );
    return person;
}
