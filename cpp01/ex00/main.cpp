#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <unistd.h>
#include "Zombie.hpp"

static 
std::string nameGenerator( void ) {

	srand(time(NULL));
	static const std::string name[21] = {
		"Kate", "Pauline", "Karine", "Carolina", "Chewie", "Mouthie", "Fetid",
		"Nomnom", "Chawchaw", "Rancie", "Dumdum", "Champ", "Glutglut", "Stinky",
		"Munchie", "Eorgh", "Bitemark", "Gorger", "Famine", "Blister", "Bcano"};
	return (name[std::rand() % 20]);
}

int main( void ) {

    std::cout << "\033[1;33m***********      New Zombie      ************\033[0m" << std::endl;
    std::cout << std::endl;
    {
        Zombie* part2 = newZombie( nameGenerator() );
        part2->announce();
        delete part2;
    }
    sleep( 1 );
    std::cout << std::endl;
    std::cout << "\033[1;33m***********  Random Chump  void  ************\033[0m" << std::endl;
    std::cout << std::endl;
    {
        randomChump( );
    }
    sleep( 1 );
    std::cout << std::endl;
    std::cout << "\033[1;33m***********  Random Chump named  ************\033[0m" << std::endl;
    std::cout << std::endl;
    {
        randomChump( nameGenerator() );
    }
    return 0;
}
