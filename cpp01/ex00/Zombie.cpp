#include <iostream>
#include "Zombie.hpp"

Zombie::Zombie( void ) :_name("John Doe") {

    std::cout << this->_name << " is back to life among the zombies horde."<< std::endl;
    return ;
}

Zombie::Zombie( std::string name ) :_name(name) {

    std::cout << this->_name << " is back to life among the zombies horde."<< std::endl;
    return ;
}

Zombie::~Zombie() {

    std::cout << this->_name << " turn back to dust."<< std::endl;
    return ;
}

void    Zombie::nominate( std::string name ) {

    this->_name = name;
}

void    Zombie::announce( void ) const {

    std::cout << this->_name << ": BraiiiiiiinnnzzzZ ... look so delicious." << std::endl;
}
