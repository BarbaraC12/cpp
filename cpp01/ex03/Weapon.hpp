#ifndef WEAPON_HPP
# define WEAPON_HPP

#include <iostream>

class Weapon {

private:
	std::string 	_type;

public:
	Weapon( std::string newType );
	Weapon( void );
	~Weapon( );

	void			setType( std::string newType );
	std::string		&getType( void ) const ;
};

#endif