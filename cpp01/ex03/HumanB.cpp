#include "HumanB.hpp"
#include <string>

HumanB::HumanB( std::string name ) :_name(name) {

	std::cout << "\033[0;34m>>\033[0m " << _name << " create with no weapon" << std::endl;
}

HumanB::~HumanB() {

}

void    HumanB::attack( ) {

    std::cout << this->_name << " attack with their " << this->_weapon->getType() << std::endl;
}

void	HumanB::setWeapon( Weapon &weapon ) {

    this->_weapon = &weapon;
}

std::string HumanB::getName( void ) const {

    return this->_name;
}