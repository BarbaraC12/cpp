#include "Weapon.hpp"

Weapon::Weapon( std::string name ) :_type(name) {

}

Weapon::Weapon( void ) :_type("fist") {

}


Weapon::~Weapon() {

}

void    Weapon::setType( std::string newType ) {
    
    this->_type = newType;
}

std::string& Weapon::getType( void ) const {

    return (this->_type);
}
