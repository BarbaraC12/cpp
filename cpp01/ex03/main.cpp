#include <cstdlib>
#include <ctime>
#include <cstdlib>
#include <unistd.h>
#include "HumanA.hpp"
#include "HumanB.hpp"
#include "Weapon.hpp"

static 
std::string nameGenerator( void ) {

	srand(time(NULL));
	static const std::string name[22] = {
		"Twirlbud", "Ginnattah", "Bedoo", "Ukhmuk", "Xeingem",
		"Lovedancer", "Ice Freak", "Slugslinger", "Bling-P", "Maria",
		"Nekoda","JacynthVampyre", "Christophoros", "Notus", "Gaios",
		"Eutychos", "Hyginos", "Zelpha", "Hugo", "Bernice", "Barbara"};

	return (name[std::rand() % 22]);
}

static 
std::string weaponGenerator( void ) {

	srand(time(NULL));
	static const std::string name[13] = {
		"sword", "pacificator", "warhammer", "battle axe", "dagger", "bow",
		"fire ball", "fist", "AK-47", "cross bow", "speare", "slingshot"};

	return (name[std::rand() % 13]);
}

int main( void ) {

	std::cout << "\033[1;33m****** HUMAN A ******\033[0m" << std::endl;
	{
		Weapon club = Weapon( weaponGenerator( ) );
		HumanA human1( nameGenerator( ), club);
		human1.attack();
		std::cout << "\033[0;34m>>\033[0m Set weapon 'some other type of club' to "<< human1.getName() << std::endl;
		club.setType("some other type of club");
		human1.attack();
	}
    sleep( 1 );
	std::cout << std::endl;
	std::cout << "\033[1;33m****** HUMAN B ******\033[0m" << std::endl;
	{
		Weapon club = Weapon( weaponGenerator( ) );
		HumanB human2( nameGenerator( ) );
		human2.setWeapon(club);
		human2.attack();
		std::cout << "\033[0;34m>>\033[0m Set weapon 'some other type of club' to "<< human2.getName() << std::endl;
		club.setType("some other type of club");
		human2.attack();
	}
	return 0;
}