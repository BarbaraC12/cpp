#include "HumanA.hpp"

HumanA::HumanA( std::string name, Weapon &weapon ) :_weapon(weapon), _name(name)  {

	std::cout << "\033[0;34m>>\033[0m " << _name << " create with '" << weapon.getType() << "'" << std::endl;
}

HumanA::~HumanA( void ) {
	
}

void    HumanA::attack( void ) {

	std::cout << this->_name << " attack with their " << this->_weapon.getType() << std::endl;
}

std::string HumanA::getName( void ) const {

	return this->_name;
}