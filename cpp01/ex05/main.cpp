#include "Karen.hpp"

int main( void ) {

    Karen   karen;
    std::string levelD = "DEBUG";
    std::string levelI = "INFO";
    std::string levelW = "WARNING";
    std::string levelE = "ERROR";
    std::cout << "\033[1;34mKaren level 1: \033[1;31m" << levelD << "\033[0m" << std::endl;
    karen.complain( levelD );
    std::cout << "\033[1;34mKaren level 2: \033[1;31m" << levelI << "\033[0m" << std::endl;
    karen.complain( levelI );
    std::cout << "\033[1;34mKaren level 3: \033[1;31m" << levelW << "\033[0m" << std::endl;
    karen.complain( levelW );
    std::cout << "\033[1;34mKaren level 4: \033[1;31m" << levelE << "\033[0m" << std::endl;
    karen.complain( levelE );
    std::cout << "\033[1;34mKaren non-existent level: \033[1;31mINVALID\033[0m" << std::endl;
    karen.complain( "INVALID" );
    return 0;
}