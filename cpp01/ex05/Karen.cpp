#include "Karen.hpp"

Karen::Karen( void ) {

	std::cout << "\033[0;32m=> Oh no! Karen is coming, shut up!\033[0m" << std::endl;
}

Karen::~Karen() {

   	std::cout << "\033[0;31m=> Phew .. Karen is finally gone!\033[0m" << std::endl;
}

void	Karen::_debug( void ) {

    std::cout << "I love having extra bacon for my 7XL-double-cheese-triple-pickle-specialketchup burger. I really do !" << std::endl;
}

void	Karen::_info( void ) {

    std::cout << "I cannot believe adding extra bacon costs more money. You didn't put enough bacon in my burger ! If you did, I wouldn't be asking for more !" << std::endl;
}

void	Karen::_warning( void ) {

    std::cout << "I think I deserve to have some extra bacon for free. I've been coming for years whereas you started working here since last month." << std::endl;
}

void	Karen::_error( void ) {

    std::cout << "This is unacceptable ! I want to speak to the manager now!" << std::endl;
}

int Karen::executeNow(std::string a, std::string b, void (Karen::*func)())
{
    int valid( 0 );

    if(a == b)
    {
        valid++;
        (this->*func)();
    }
    return valid;
}

void	Karen::complain( std::string level ) {

    int valid( 0 );

    valid += executeNow( level, "DEBUG", &Karen::_debug );
    valid += executeNow( level, "INFO", &Karen::_info );
    valid += executeNow( level, "WARNING", &Karen::_warning );
    valid += executeNow( level, "ERROR", &Karen::_error );

    if ( valid != 1 )
        std::cout << "** Shhh make fc! Karen is hidding somewhere" << std::endl;
}
