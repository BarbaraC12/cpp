#include <fstream>
#include <iostream>
#include <cstring>
#include <sstream>

std::string	myReplace(std::string filename, const std::string& s1, const std::string& s2)
{
	size_t	start_pos;

	start_pos = 0;
	while((start_pos = filename.find(s1, start_pos)) != std::string::npos)
	{
		filename.insert(start_pos, s2);
		start_pos += s2.length();
		filename.erase(start_pos, s1.length());
	}
	return filename;
}

int	main(int argc, char **argv)
{
	std::stringstream	tmp;
	std::string			s1, s2, filename;
	std::ofstream		outfile;
	std::ifstream		infile;

	if (argc != 4)
	{
		std::cerr << "\033[1;31m> \033[0;m./sed: Error: wrong number of arguments" << std::endl;
		std::cerr << "Usage: ./sed resume Arthur Toto" << std::endl;
		return (1);
	}
	if (std::strlen(argv[2]) < 1)
	{
		std::cerr << "\033[1;31m> \033[0;m./sed: Error: string to replace can't be empty" << std::endl;
		return (2);
	}
	filename = argv[1]; 
	infile.open(filename.c_str());
	if (infile.is_open())
		tmp << infile.rdbuf();
	else
	{
		std::cerr << "\033[1;31m> \033[0;m./sed: Error: failure open: " << filename << std::endl;
		return (3);
	}
	filename.append(".replace");
	outfile.open(filename.c_str());
	if (outfile.is_open())
		std::cerr << "\033[1;32m> \033[0;mSuccess: file create: " << filename << std::endl;
	else
	{
		std::cerr << "\033[1;31m> \033[0;m./sed: Error: failure create: " << filename << std::endl;
		return (4);
	}
	s1 = std::string(argv[2]);
	s2 = std::string(argv[3]);
	outfile << myReplace(tmp.str(), s1, s2);
	outfile.close();
	infile.close();
	return (0);
}