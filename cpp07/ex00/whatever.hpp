#ifndef WHATEVER_HPP
# define WHATEVER_HPP

# include <iostream>
# include <string>

template< typename T >
void		swap( T & x, T & y ) {

	T temp( x );
	x = y;
	y = temp;
	return ;
}

template< typename T >
T const		&max( T const & x, T const & y ) {

	return ( x > y ? x : y );
}

template< typename T >
T const		&min( T const & x, T const & y ) {

	return ( x < y ? x : y );
}

// class Awesome
// {
// private:
// 	int _n;
// public:
// 	Awesome(void):_n(0) {}
// 	Awesome(int n):_n(n) {}
// 	Awesome & operator=(Awesome & a) { _n = a._n; return *this; }
// 	bool operator==(Awesome const & rhs) const { return(this->_n == rhs._n); }
// 	bool operator!=(Awesome const & rhs) const { return(this->_n != rhs._n); }
// 	bool operator<(Awesome const & rhs) const { return(this->_n < rhs._n); }
// 	bool operator<=(Awesome const & rhs) const { return(this->_n <= rhs._n); }
// 	bool operator>(Awesome const & rhs) const { return(this->_n > rhs._n); }
// 	bool operator>=(Awesome const & rhs) const { return(this->_n >= rhs._n); }

// 	int	get_n(void) const { return _n; }
// };

// std::ostream & operator<< (std::ostream & o, const Awesome & a) { o << a.get_n(); return o; }


#endif