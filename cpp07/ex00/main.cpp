#include "whatever.hpp"

int		test( int x ) {

	std::cout << "Function test call here: return = " << x << std::endl;
	return x;
}

int		main() {

	std::cout << "\033[1;33m------------ TEST INTEGER\033[0m\n";
	int forty_two(21);
	int twenty_one(42);

	std::cout << "Init  : forty-two = " << forty_two << ", twenty-one = " << twenty_one << std::endl;
	::swap( forty_two, twenty_one );
	std::cout << "Swap  : forty-two = " << forty_two << ", twenty-one = " << twenty_one << std::endl;
	std::cout << "min   : " << ::min( forty_two, twenty_one ) << std::endl;
	std::cout << "max   : " << ::max( forty_two, twenty_one ) << std::endl;
	
	std::cout << "\033[1;33m----------- TEST FUNCTION\033[0m\n";
	{
		int ret( ::max( test(twenty_one), test(forty_two) ));
		std::cout << "max   : " << ret << std::endl;
	}

	std::cout << "\033[1;33m------------- TEST STRING\033[0m\n";
	{
		std::string a("def");
		std::string b("abc");

		std::cout << "Init  : a = " << a << ", b = " << b << std::endl;
		::swap(a, b);
		std::cout << "Swap  : a = " << a << ", b = " << b << std::endl;
		std::cout << "min   : " << ::min( a, b ) << std::endl;
		std::cout << "max   : " << ::max( a, b ) << std::endl;
	}
	std::cout << "\033[1;33m-------------- TEST FLOAT\033[0m\n";
        {
                float a(-0.6f);
                float b(42.1f);

                std::cout << "Init  : a = " << a << ", b = " << b << std::endl;
                ::swap(a, b);
                std::cout << "Swap  : a = " << a << ", b = " << b << std::endl;
                std::cout << "min   : " << ::min( a, b ) << std::endl;
                std::cout << "max   : " << ::max( a, b ) << std::endl;
        }

	std::cout << "\033[1;33m-------------- TEST FLOAT -0.0f <-> 0.0f\033[0m\n";
	{
		float a(-0.0f);
		float b(0.0f);

		std::cout << "Init  : a = " << a << ", b = " << b << std::endl;
		::swap(a, b);
		std::cout << "Swap  : a = " << a << ", b = " << b << std::endl;
		std::cout << "min   : " << ::min( a, b ) << std::endl;
		std::cout << "max   : " << ::max( a, b ) << std::endl;
	}
}
// int main(){
	// Awesome a(2), b(4); 
	// std::cout << "a = " << a << ", b = " << b << std::endl;
	// swap(a, b);
	// std::cout << "a = " << a << ", b = " << b << std::endl;
// }
