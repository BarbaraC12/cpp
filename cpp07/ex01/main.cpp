#include "iter.hpp"

int		main() {

	int			tab[] = { 0, 1, 2, 3, 4 };
	int			tab1[1] = {0};
	const char	tab2[] = { 'a', 'b', 'c', 'd', 'z' };

	std::cout << "\033[1;33m------------- INT TEST\033[0m\n";
	::iter( tab, 5, printer);
	std::cout << "\033[1;33m------------ CHAR TEST\033[0m\n";
	::iter( tab2, 5, printer);
	std::cout << "\033[1;33m------------- ADD TEST\033[0m\n";
	::iter( tab1, 1, add);
	std::cout << std::endl;

	return 0;
}
