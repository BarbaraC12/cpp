#ifndef ITER_HPP
# define ITER_HPP

# include <iostream>
# include <string>

template< typename T >
void		printer( T & x ) {

	std::cout << "| " << x << " |\n";
}

template< typename T >
void		add( T & x ) {

	std::cout << "Before add   : ";
	::printer( x );
	x += 42;
	std::cout << "After add 42 : ";
	::printer( x );
}

template< typename T>
void		iter( T *str, std::size_t const len, void (*f)( T & ) ) {

	for ( std::size_t i(0); i < len; i++ )
		f( str[i] );
}



#endif
