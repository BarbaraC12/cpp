#ifndef ARRAY_HPP
# define ARRAY_HPP

# include <iostream>
# include <string>
# include <cstdlib>
# include <time.h>

template< typename T >
class Array {

		unsigned int	_size;
		T *				_ptr;

	public:

		Array<T>( unsigned int n = 0U) :_size(n), _ptr( new T[n]() ) { 
			std::cout << "\033[1;32m>> I \033[0mNew array create --- SIZE : " << this->_size << std::endl;
			return ; };
		
		Array<T>( Array<T> const & src ) : _size(src._size), _ptr( new T[src.size()] ) {
			for ( unsigned int i(0); i < this->_size; i++ )
				this->_ptr[i] = src._ptr[i];
			std::cout << "\033[1;32m>> C \033[0mNew copy array  ---- SIZE : " << this->_size << std::endl;
			return ; };
		
		~Array<T>( void ) {
			std::cout << "\033[1;31m<< D \033[0mArray Destroy  ----- SIZE : " << this->_size << std::endl;
			delete [] this->_ptr;
			return ; };
		
		Array<T> &	operator=( const Array<T> & rhs ) {
			if ( this != &rhs )
			{
				this->_size = rhs.size();
				delete[] this->_ptr;
				this->_ptr = new T[this->_size]();
				for ( unsigned int i(0); i < this->_size; i++ )
					this->_ptr[i] = rhs._ptr[i];
			}
			return *this ; };
		
		T &			operator[]( unsigned int id ) {
			if ( id >= this->_size ) {
				std::cout << "Called ID : " << id << std::endl;
				throw InvalidIdException(); }
			return this->_ptr[id]; };
		
		unsigned int	size() const { return this->_size ; };

		class	InvalidIdException: public std::exception {
			public:
				virtual const char* what() const throw() {
					return ("\033[1;35m!! E \033[0mIndex called is invalid");
				}
		};

};

template< typename T >
std::ostream &operator<<(std::ostream &o, Array<T> &src ) {
	o << "\033[1;33m   ------- Array -------\033[0m\n";
	for ( unsigned int i(0); i < src.size(); i++)
		o << "\033[0;33m .\033[0m tab[ " << i << " ]:\t" << src[i] << std::endl; 
	return o ; };

#endif
