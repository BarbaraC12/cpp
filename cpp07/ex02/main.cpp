#include <iostream>
#include "Array.hpp"

#define MAX_VAL 10
int main(int, char**) {

	Array<int> numbers(MAX_VAL);
	int* mirror = new int[MAX_VAL];
	srand(time(NULL));

	std::cout << "BEFORE ANYTHING" << std::endl;
	std::cout << numbers;
	for (int i = 0; i < MAX_VAL; i++) {

		const int value = rand();
		numbers[i] = value;
		mirror[i] = value;
	}
	std::cout << "FROM\n" << numbers;
	
		Array<int> tmp = numbers;
		std::cout << "COPY TMP\n" << tmp;
		Array<int> test(tmp);
		std::cout << "COPY TEST\n" << test;
	

	for (int i = 0; i < MAX_VAL; i++) {

		if (test[i] != numbers[i]) {
			std::cerr << "didn't save the same value!!" << std::endl;
			return 1;
		}
	}

	numbers[1] *= -1;

	std::cout << "Little verif 1.0\n" << numbers;

	std::cout << "Little verif 1.1\n" << test;

	try {
		numbers[-2] = 0;
	}
	catch(const std::exception& e) {
		std::cerr << e.what() << '\n';
	}

	try {
		numbers[MAX_VAL] = 0;
	}
	catch(const std::exception& e) {
		std::cerr << e.what() << '\n';
	}

	for (int i = 0; i < MAX_VAL; i++) {
		numbers[i] = rand();
	}
	delete [] mirror;
	return 0;
}
